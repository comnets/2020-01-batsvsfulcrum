#! /usr/bin/env python3
# encoding: utf-8

import matplotlib.pyplot as plt
import math
import numpy as np

def main():
    K =64#512#32#256

    

    ####################################################################
    with open('simDegreeK'+str(K)+'M32m8.txt','r') as fl:
        data=[float(x) for x in fl.read().split()]
    print ("K"+str(K)+" data length",len(data))

    local = []
    for i in range(len(data)): 
        if data[i] !=  0.0000000e+00:
            # print(data[i])
            local.append(i)
    print("local Non Zero K1600:",local)
    average_degree=0
    for i in range(len(local)):
        average_degree += (i+1)*data[local[i]]

    print("average degree:",average_degree)




if __name__ == "__main__":
    main()