#! /usr/bin/env python3
# encoding: utf-8

import matplotlib.pyplot as plt
import math
import numpy as np

def main():
    K =256#512#32#256

    # label_arry = ['delta=1000','delta=100','delta=10','delta=1','delta=0.1','delta=0.01','delta=0.001','delta=0.0001','delta=0.00001']
    # label_arry = ['delta=1000','delta=10','delta=0.1','delta=0.001','delta=0.00001']
    # label_arry = ['delta=1000','delta=10','delta=0.1','delta=0.002','delta=0.00001','delta=0.0000x']
    # label_arry = ['delta=1000','delta=0.1','delta=0.00001','delta=0.0000x']
    label_arry = ['delta=10','delta=0.1','delta=0.0001','delta=0.00001']



    marker_arry = ['*','o','P','x','+','1','v','2','3','4','5','8','9','.']

    ####################################################################
    with open('simDegreeK1600M32m8.txt','r') as fl:
        data=[float(x) for x in fl.read().split()]
    print ("K1600 data length",len(data))

    local = []
    for i in range(len(data)): 
        if data[i] !=  0.0000000e+00:
            # print(data[i])
            local.append(i)
    print("local Non Zero K1600:",local)
    average_degree=0
    for i in range(len(local)):
        average_degree += (i+1)*data[local[i]]

    print("average degree:",average_degree)


   

    #########
    with open('simDegreeK8000M32m8.txt','r') as fl:
        data=[float(x) for x in fl.read().split()]
    print ("K8000 data length",len(data))
    
    local = []
    for i in range(len(data)): 
        if data[i]!=0.0000000e+00:
            local.append(i)
    print("local Non Zero K8000:",local)

    for i in range(len(local)):
        average_degree += (i+1)*data[local[i]]

    print("average degree:",average_degree)

    ########
    with open('simDegreeK16000M32m8.txt','r') as fl:
        data=[float(x) for x in fl.read().split()]
    print ("K160000 data length",len(data))
    
    local = []
    for i in range(len(data)): 
        if data[i]!= 0.0000000e+00:
            # print(data[i])
            local.append(i)
    print("local Non Zero K16000:",local)
    for i in range(len(local)):
        average_degree += (i+1)*data[local[i]]

    print("average degree:",average_degree)



    # with open('simDegreeK64M32m8.txt','r') as fl:
    #     data=[float(x) for x in fl.read().split()]
    # print ("K64 data length",len(data))
    
    # local = []
    # for i in range(len(data)): 
    #     if data[i]!= 0.0000000e+00:
    #         # print(data[i])
    #         local.append(i)
    # print("local Non Zero K64:",local)
    ################################################################
    data = []

    number = len(label_arry)
    for i in range(1,number+1):
        fileName="simDegreeK"+str(K)+"M32m8delta"+str(i)+".txt"
        with open(fileName,'r') as fl:
            data.append([float(item) for item in fl.read().split()])
        
        # print(data)

    # fig,ax = plt.subplot()
    plt.subplot()

    
    t = np.arange(20)
    p_array=[]
    for i in range(number):
        p_temp,=plt.plot(data[i][:30],marker=marker_arry[i],label=label_arry[i])
        # p_temp,=plt.plot(data[i][:30],marker=marker_arry[i])

        p_array.append(p_temp)
    # ax.plot(t,data[0][:20],'*',t,data[1][:20],'o')
    # ax.plot()
    # ax.set_xlim(0, 5)
    # ax.set_ylabel('possibility')
    # ax.set_xlabel('items')
    # ax.grid(True)
    plt.grid(True)
    # plt.legend(loc='upper right', borderaxespad=0.)
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()