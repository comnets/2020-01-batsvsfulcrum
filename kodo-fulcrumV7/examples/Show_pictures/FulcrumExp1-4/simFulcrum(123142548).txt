expansion: 1
symbols_item: 16 Encoder 1668.44 MB/s Recoder -nan MB/s InnerDecoder 1631.49 MB/s OuterDecoder 789.76 MB/s CombiDecoder 1083.69 MB/s 
symbols_item: 32 Encoder 955.054 MB/s Recoder -nan MB/s InnerDecoder 875.406 MB/s OuterDecoder 458.446 MB/s CombiDecoder 678.507 MB/s 
symbols_item: 64 Encoder 492.862 MB/s Recoder -nan MB/s InnerDecoder 437.671 MB/s OuterDecoder 231.861 MB/s CombiDecoder 368.301 MB/s 
symbols_item: 128 Encoder 265.577 MB/s Recoder -nan MB/s InnerDecoder 237.19 MB/s OuterDecoder 119.47 MB/s CombiDecoder 207.975 MB/s 
symbols_item: 256 Encoder 130.91 MB/s Recoder -nan MB/s InnerDecoder 117.163 MB/s OuterDecoder 54.6148 MB/s CombiDecoder 105.496 MB/s 
symbols_item: 512 Encoder 61.0289 MB/s Recoder -nan MB/s InnerDecoder 55.6204 MB/s OuterDecoder 24.7727 MB/s CombiDecoder 51.238 MB/s 
symbols_item: 1024 Encoder 24.7131 MB/s Recoder -nan MB/s InnerDecoder 21.8664 MB/s OuterDecoder 8.91838 MB/s CombiDecoder 20.4414 MB/s 
expansion: 2
symbols_item: 16 Encoder 1897.05 MB/s Recoder -nan MB/s InnerDecoder 1831.41 MB/s OuterDecoder 858.432 MB/s CombiDecoder 1131.71 MB/s 
symbols_item: 32 Encoder 974.148 MB/s Recoder -nan MB/s InnerDecoder 898.513 MB/s OuterDecoder 436.091 MB/s CombiDecoder 663.898 MB/s 
symbols_item: 64 Encoder 545.304 MB/s Recoder -nan MB/s InnerDecoder 478.859 MB/s OuterDecoder 232.356 MB/s CombiDecoder 392.92 MB/s 
symbols_item: 128 Encoder 259.722 MB/s Recoder -nan MB/s InnerDecoder 232.29 MB/s OuterDecoder 105.057 MB/s CombiDecoder 200.119 MB/s 
symbols_item: 256 Encoder 130.837 MB/s Recoder -nan MB/s InnerDecoder 117.834 MB/s OuterDecoder 49.9588 MB/s CombiDecoder 105.234 MB/s 
symbols_item: 512 Encoder 61.1485 MB/s Recoder -nan MB/s InnerDecoder 55.7875 MB/s OuterDecoder 22.3827 MB/s CombiDecoder 51.0685 MB/s 
symbols_item: 1024 Encoder 24.5615 MB/s Recoder -nan MB/s InnerDecoder 21.7966 MB/s OuterDecoder 8.19799 MB/s CombiDecoder 20.3257 MB/s 
expansion: 3
symbols_item: 16 Encoder 1617.19 MB/s Recoder -nan MB/s InnerDecoder 1551.42 MB/s OuterDecoder 706.703 MB/s CombiDecoder 907.025 MB/s 
symbols_item: 32 Encoder 1022.28 MB/s Recoder -nan MB/s InnerDecoder 935.605 MB/s OuterDecoder 446.591 MB/s CombiDecoder 666.206 MB/s 
symbols_item: 64 Encoder 525.966 MB/s Recoder -nan MB/s InnerDecoder 462.763 MB/s OuterDecoder 215.307 MB/s CombiDecoder 369.262 MB/s 
symbols_item: 128 Encoder 261.954 MB/s Recoder -nan MB/s InnerDecoder 234.123 MB/s OuterDecoder 101.375 MB/s CombiDecoder 198.317 MB/s 
symbols_item: 256 Encoder 123.128 MB/s Recoder -nan MB/s InnerDecoder 111.517 MB/s OuterDecoder 44.433 MB/s CombiDecoder 98.6715 MB/s 
symbols_item: 512 Encoder 60.6132 MB/s Recoder -nan MB/s InnerDecoder 55.281 MB/s OuterDecoder 21.0912 MB/s CombiDecoder 50.3283 MB/s 
symbols_item: 1024 Encoder 24.4374 MB/s Recoder -nan MB/s InnerDecoder 21.7008 MB/s OuterDecoder 7.86823 MB/s CombiDecoder 20.1782 MB/s 
expansion: 4
symbols_item: 16 Encoder 1565.2 MB/s Recoder -nan MB/s InnerDecoder 1498.79 MB/s OuterDecoder 697.188 MB/s CombiDecoder 832.162 MB/s 
symbols_item: 32 Encoder 867.176 MB/s Recoder -nan MB/s InnerDecoder 802.455 MB/s OuterDecoder 372.415 MB/s CombiDecoder 546.228 MB/s 
symbols_item: 64 Encoder 484.042 MB/s Recoder -nan MB/s InnerDecoder 429.2 MB/s OuterDecoder 194.985 MB/s CombiDecoder 332.751 MB/s 
symbols_item: 128 Encoder 246.205 MB/s Recoder -nan MB/s InnerDecoder 221.534 MB/s OuterDecoder 92.9325 MB/s CombiDecoder 184.1 MB/s 
symbols_item: 256 Encoder 123.769 MB/s Recoder -nan MB/s InnerDecoder 112.256 MB/s OuterDecoder 43.7169 MB/s CombiDecoder 98.0941 MB/s 
symbols_item: 512 Encoder 58.2988 MB/s Recoder -nan MB/s InnerDecoder 53.3303 MB/s OuterDecoder 19.7669 MB/s CombiDecoder 48.298 MB/s 
symbols_item: 1024 Encoder 24.5898 MB/s Recoder -nan MB/s InnerDecoder 21.8532 MB/s OuterDecoder 7.73492 MB/s CombiDecoder 20.2426 MB/s 
################# 
Packet Num:16, 32, 64, 128, 256, 512, 1024, 
Sybmol size of symbols array: 7

Encoder Throughput: [ [expansion 1], [expansion 2], [expansion 3], [expansion 4] ]
[[1668.44, 955.054, 492.862, 265.577, 130.91, 61.0289, 24.7131], 
[1897.05, 974.148, 545.304, 259.722, 130.837, 61.1485, 24.5615], 
[1617.19, 1022.28, 525.966, 261.954, 123.128, 60.6132, 24.4374], 
[1565.2, 867.176, 484.042, 246.205, 123.769, 58.2988, 24.5898]]
Inner_Decoder=[[1631.49, 875.406, 437.671, 237.19, 117.163, 55.6204, 21.8664],
[1831.41, 898.513, 478.859, 232.29, 117.834, 55.7875, 21.7966],
[1551.42, 935.605, 462.763, 234.123, 111.517, 55.281, 21.7008],
[1498.79, 802.455, 429.2, 221.534, 112.256, 53.3303, 21.8532]]
Outer_Decoder=[[789.76, 458.446, 231.861, 119.47, 54.6148, 24.7727, 8.91838],
[858.432, 436.091, 232.356, 105.057, 49.9588, 22.3827, 8.19799],
[706.703, 446.591, 215.307, 101.375, 44.433, 21.0912, 7.86823],
[697.188, 372.415, 194.985, 92.9325, 43.7169, 19.7669, 7.73492]]
Combi_Decoder=[[1083.69, 678.507, 368.301, 207.975, 105.496, 51.238, 20.4414],
[1131.71, 663.898, 392.92, 200.119, 105.234, 51.0685, 20.3257],
[907.025, 666.206, 369.262, 198.317, 98.6715, 50.3283, 20.1782],
[832.162, 546.228, 332.751, 184.1, 98.0941, 48.298, 20.2426]]
