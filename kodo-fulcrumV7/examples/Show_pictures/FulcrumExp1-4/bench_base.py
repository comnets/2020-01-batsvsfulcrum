#! /usr/bin/env python3
# encoding: utf-8

import matplotlib.pyplot as plt 

def show(Data):
    
    return

def main():
    # fileName="kodo_bench_20191125"
    fileName="kodo_bench"

    with open(fileName+".txt",'r') as fin:
        inputData=[float(item) for item in fin.read().split() ]

    # get from txt
    runLoop=50
    tempResult=[]
    for i in range(len(inputData)//runLoop):
        # start=item*1000
        # stop=item*1000+1000
        tempData=[inputData[j] for j in range(i*runLoop,(i+1)*runLoop)]
        tempResult.append(sum(tempData)/runLoop)

    # # get the average result from two dimension result
    # result=[]
    # for i in range(len(tempResult)):
    #     result.append(sum(tempResult[i])/1000)

    # 16 32 64 128 256 512 1024
    lenEncDec=len(tempResult)//3
    # for i in range(len(tempResult)):

    innerEncDec=[tempResult[item]  for item in range(lenEncDec)]
    outerEncDec=[tempResult[item]  for item in range(lenEncDec,2*lenEncDec)]
    combiEncDec=[tempResult[item]  for item in range(2*lenEncDec,3*lenEncDec)]

    # exp = 4 # len(1 2 3 4)
    # k = 7 # len(16 32 64 128 256 512 1024)
    innerEnc=[]
    innerDec=[]
    outerEnc=[]
    outerDec=[]
    combiEnc=[]
    combiDec=[]
    for exp in range(4):
        for k in range(7):
            innerEnc.append(innerEncDec[k*8+exp])
            innerDec.append(innerEncDec[4+k*8+exp])

            outerEnc.append(outerEncDec[k*8+exp])
            outerDec.append(outerEncDec[4+k*8+exp])

            combiEnc.append(combiEncDec[k*8+exp])
            combiDec.append(combiEncDec[4+k*8+exp])


    nameArray=["Inner_Encoder=","Outer_Encoder=","Combi_Encoder=","Inner_Decoder=","Outer_Decoder=","Combi_Decoder="]
    EncDecArray=[innerEnc,outerEnc,combiEnc,innerDec,outerDec,combiDec]

    for item in range(len(nameArray)):
        # print(nameArray[item],end='')
        print(nameArray[item],end='')

        for i in range(28):
            if(i==0):
                print("[[",end='')
            
            if(i<28-1):
                print(format(EncDecArray[item][i],'.3f'),end='')
                if((i+1)%7==0):
                    print("],")
                    print("[",end='')
                else:
                    print(", ",end='')
            
            if(i==28-1):
                    print(format(EncDecArray[item][i],'.3f'),"]]")
            
  



    print("tempResult length: ",len(tempResult))
    # print("result length: ",len(result))
    print("innerEncDec length: ",len(innerEncDec))
    

if __name__ == "__main__":
    main()
