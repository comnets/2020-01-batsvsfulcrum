expansion: 1
symbols_item: 16 Encoder 1034.85 MB/s Recoder 400.534 MB/s Decoder 449.75 MB/s 
symbols_item: 32 Encoder 754.799 MB/s Recoder 307.158 MB/s Decoder 347.888 MB/s 
symbols_item: 64 Encoder 372.737 MB/s Recoder 156.24 MB/s Decoder 164.185 MB/s 
symbols_item: 128 Encoder 229.835 MB/s Recoder 101.621 MB/s Decoder 95.5695 MB/s 
symbols_item: 256 Encoder 93.3863 MB/s Recoder 43.4336 MB/s Decoder 38.6456 MB/s 
symbols_item: 512 Encoder 51.4674 MB/s Recoder 24.4139 MB/s Decoder 20.0672 MB/s 
symbols_item: 1024 Encoder 22.6614 MB/s Recoder 10.995 MB/s Decoder 8.38 MB/s 
expansion: 2
symbols_item: 16 Encoder 1317.15 MB/s Recoder 512.168 MB/s Decoder 553.316 MB/s 
symbols_item: 32 Encoder 854.704 MB/s Recoder 340.644 MB/s Decoder 352.45 MB/s 
symbols_item: 64 Encoder 425.703 MB/s Recoder 174.375 MB/s Decoder 162.299 MB/s 
symbols_item: 128 Encoder 219.45 MB/s Recoder 95.9064 MB/s Decoder 79.0944 MB/s 
symbols_item: 256 Encoder 129.741 MB/s Recoder 56.6995 MB/s Decoder 45.0733 MB/s 
symbols_item: 512 Encoder 61.94 MB/s Recoder 28.1144 MB/s Decoder 20.5845 MB/s 
symbols_item: 1024 Encoder 24.7833 MB/s Recoder 12.0136 MB/s Decoder 8.39129 MB/s 
expansion: 3
symbols_item: 16 Encoder 1337.04 MB/s Recoder 509.063 MB/s Decoder 532.643 MB/s 
symbols_item: 32 Encoder 895.09 MB/s Recoder 356.676 MB/s Decoder 356.332 MB/s 
symbols_item: 64 Encoder 517.774 MB/s Recoder 208.006 MB/s Decoder 194.032 MB/s 
symbols_item: 128 Encoder 265.118 MB/s Recoder 114.2 MB/s Decoder 94.3378 MB/s 
symbols_item: 256 Encoder 131.166 MB/s Recoder 57.3047 MB/s Decoder 43.4543 MB/s 
symbols_item: 512 Encoder 61.721 MB/s Recoder 28.074 MB/s Decoder 19.6068 MB/s 
symbols_item: 1024 Encoder 25.1256 MB/s Recoder 12.1641 MB/s Decoder 8.19631 MB/s 
expansion: 4
symbols_item: 16 Encoder 1413.39 MB/s Recoder 535.507 MB/s Decoder 582.439 MB/s 
symbols_item: 32 Encoder 759.515 MB/s Recoder 302.346 MB/s Decoder 296.14 MB/s 
symbols_item: 64 Encoder 506.466 MB/s Recoder 203.89 MB/s Decoder 187.734 MB/s 
symbols_item: 128 Encoder 230.098 MB/s Recoder 99.6859 MB/s Decoder 78.4397 MB/s 
symbols_item: 256 Encoder 132.898 MB/s Recoder 58.2238 MB/s Decoder 43.2422 MB/s 
symbols_item: 512 Encoder 55.3071 MB/s Recoder 25.7971 MB/s Decoder 17.6769 MB/s 
symbols_item: 1024 Encoder 20.2238 MB/s Recoder 9.74589 MB/s Decoder 6.44937 MB/s 
################# 
Packet Num:16, 32, 64, 128, 256, 512, 1024, 
Sybmol size of symbols array: 7

Encoder Throughput :
[[1034.85, 754.799, 372.737, 229.835, 93.3863, 51.4674, 22.6614], 
[1317.15, 854.704, 425.703, 219.45, 129.741, 61.94, 24.7833], 
[1337.04, 895.09, 517.774, 265.118, 131.166, 61.721, 25.1256], 
[1413.39, 759.515, 506.466, 230.098, 132.898, 55.3071, 20.2238]]

Decoder Throughput :[ [expansion 1], [expansion 2], [expansion 3], [expansion 4] ]
[[449.75, 347.888, 164.185, 95.5695, 38.6456, 20.0672, 8.38],
[553.316, 352.45, 162.299, 79.0944, 45.0733, 20.5845, 8.39129],
[532.643, 356.332, 194.032, 94.3378, 43.4543, 19.6068, 8.19631],
[582.439, 296.14, 187.734, 78.4397, 43.2422, 17.6769, 6.44937]]
