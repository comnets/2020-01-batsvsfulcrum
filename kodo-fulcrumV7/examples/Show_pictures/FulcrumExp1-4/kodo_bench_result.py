#! /usr/bin/env python3
# encoding: utf-8

import matplotlib.pyplot as plt 
#from out_50 import data
# from out26 import data
from out_5500 import data


def show(Data):
    
    return

def main():
    
    # import csv

    # with open('out_20191125.csv', 'r') as f:
    #     reader = csv.reader(f)
    #     data = list(reader)

    
    print("Total rows: ", len(data))
    lenEncDec=len(data)//3

    runLoop=len(data[0]['run_number'])
    print("Run times: ", runLoop)

    # 16 32 64 128 256 512 1024
    

    # for i in range(len(tempResult)):

    innerEncDec=[sum(data[item]["throughput"])/runLoop  for item in range(lenEncDec)]
    outerEncDec=[sum(data[item]["throughput"])/runLoop  for item in range(lenEncDec,2*lenEncDec)]
    combiEncDec=[sum(data[item]["throughput"])/runLoop  for item in range(2*lenEncDec,3*lenEncDec)]

    # exp = 4 # len(1 2 3 4)
    # k = 7 # len(16 32 64 128 256 512 1024)
    innerEnc=[]
    innerDec=[]
    outerEnc=[]
    outerDec=[]
    combiEnc=[]
    combiDec=[]
    for exp in range(4):
        for k in range(7):
            innerEnc.append(innerEncDec[k*8+exp])
            innerDec.append(innerEncDec[4+k*8+exp])

            outerEnc.append(outerEncDec[k*8+exp])
            outerDec.append(outerEncDec[4+k*8+exp])

            combiEnc.append(combiEncDec[k*8+exp])
            combiDec.append(combiEncDec[4+k*8+exp])


    nameArray=["Inner_Encoder=","Outer_Encoder=","Combi_Encoder=","Inner_Decoder=","Outer_Decoder=","Combi_Decoder="]
    EncDecArray=[innerEnc,outerEnc,combiEnc,innerDec,outerDec,combiDec]

    for item in range(len(nameArray)):
        # print(nameArray[item],end='')
        print(nameArray[item],end='')

        for i in range(28):
            if(i==0):
                print("[[",end='')
            
            if(i<28-1):
                print(format(EncDecArray[item][i],'.3f'),end='')
                if((i+1)%7==0):
                    print("],")
                    print("[",end='')
                else:
                    print(", ",end='')
            
            if(i==28-1):
                    print(format(EncDecArray[item][i],'.3f'),"]]")
            
  



    # print("tempResult length: ",len(tempResult))
    # # print("result length: ",len(result))
    # print("innerEncDec length: ",len(innerEncDec))
    

if __name__ == "__main__":
    main()
