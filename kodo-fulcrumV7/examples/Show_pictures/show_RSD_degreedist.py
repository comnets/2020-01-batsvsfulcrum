#! /usr/bin/env python3
# encoding: utf-8

import matplotlib.pyplot as plt
import math
import numpy as np

def show(data,delta_array):
    dataNum=len(data)
    # if((dataNum+1)%2==0)
    #     row=(dataNum+1)2
    print("dataNum=",dataNum)

    # row=dataNum//2

    fig,ax = plt.subplots(dataNum)

    label_arry=[]
    for delta in delta_array:
        label_arry.append("delta="+str(delta))

    # label_arry =["delta=5"]
    marker_arry = ['*','o','P','x','+','1','v','2','3','4','5','8','9','.']
    color_array = ['blue','orange','green','red','purple','brown','pink','gray','olive','cyan']
         
    for i in range(dataNum):
        # ax[i].scatter(data[i],marker=marker_arry[i])
        ax[i].plot(data[i][:50],marker=marker_arry[i],color=color_array[i],label=label_arry[i])
        ax[i].legend()

        ax[i].get_xaxis().set_visible(False)

    ax[i].get_xaxis().set_visible(True)

    fig.suptitle("Robust Soliton Degree Distribution")
    plt.show()

def main():
    K =256#512#32#256
    dataSum1=[]
    delta_array=[100,5,0.5,0.1,0.05,0.01,0.005,0.001]

    for item in delta_array:
        with open('simDDK'+str(K)+'delta'+str(item)+'.txt','r') as fl:
        # with open('simDDK'+str(K)+'delta'+str(item)+'.txt','r') as fl:
            data=[float(x) for x in fl.read().split()]
            dataSum1.append(data)
            
        # print ("K"+str(K)+" data length",len(data))


    show(dataSum1,delta_array)

  

if __name__ == "__main__":
    main()