#! /usr/bin/env python3
# encoding: utf-8

import matplotlib.pyplot as plt
import math
import numpy as np
import random

def show_possibility(ddist,delta_array):
    dataNum=len(ddist)
    # if((dataNum+1)%2==0)
    #     row=(dataNum+1)2
    print("dataNum=",dataNum,"\n",
          "dataNum/2=",dataNum/2,"\n",
          "dataNum//2=",dataNum//2)

    row=dataNum//2

    fig,ax = plt.subplots(row,2)

    label_arry=[]
    for delta in delta_array:
        label_arry.append("delta="+str(delta))

    # label_arry =["delta=5"]
    marker_arry = ['*','o','P','x','+','1','v','2','3','4','5','8','9','.']
    color_array = ['blue','orange','green','red','purple','brown','pink','gray','olive','cyan']

    xachse=range(1,51)
    xachse=np.linspace(1,50,50)
    for i in range(dataNum):
        # ax[i].scatter(ddist[i],marker=marker_arry[i])
        if(i<row):
            # ax[i,0].plot(ddist[i][:50],marker=marker_arry[i],color=color_array[i],label=label_arry[i])
            yachse=[item*100 for item in ddist[i][:50]]
            
            ax[i,0].stem(xachse,yachse,label=label_arry[i]) #,linefmt=None, markerfmt=None, basefmt=None)
            # ax[i,0].stem(ddist[i],label=label_arry[i],linefmt=None, markerfmt=None, basefmt=None)

            ax[i,0].legend()
            ax[i,0].get_xaxis().set_visible(False)
        else:
            # ax[i-dataNum//2,1].plot(ddist[i][:50],marker=marker_arry[i],color=color_array[i],label=label_arry[i])
            yachse=[item*100 for item in ddist[i][:50]]
            ax[i-row,1].stem(xachse,yachse,label=label_arry[i])#),linefmt=None, markerfmt=None, basefmt=None)
            # ax[i-dataNum//2,1].stem(ddist[i],label=label_arry[i],linefmt=None, markerfmt=None, basefmt=None)
            ax[i-row,1].legend()
            ax[i-row,1].get_xaxis().set_visible(False)

    ax[row-1,0].get_xaxis().set_visible(True)
    ax[row-1,1].get_xaxis().set_visible(True)
    # ax[row-1,0].set_xlabel("Degree Number [1,50]")
    # ax[row-1,1].set_xlabel("Degree Number [1,50]")

    # fig.suptitle("Robust Soliton Degree Distribution with K=256 c=0.1")
    # ax[2,0].set_ylabel("Degree Possibility (0,1)")
    plt.show()

def show_num(ddist,delta_array,packet_n):

    dataNum=len(ddist)
    # # if((dataNum+1)%2==0)
    # #     row=(dataNum+1)2
    # print("dataNum=",dataNum,"\n",
    #       "dataNum/2=",dataNum/2,"\n",
    #       "dataNum//2=",dataNum//2)

    # # row=dataNum//2
    degree_array=[]
    for i in range(packet_n):
        degree=ddist[random.randint(0,dataNum+1)]



    fig,ax = plt.subplots(dataNum//2,2)

    label_arry=[]
    for delta in delta_array:
        label_arry.append("delta="+str(delta))

    # label_arry =["delta=5"]
    marker_arry = ['*','o','P','x','+','1','v','2','3','4','5','8','9','.']
    color_array = ['blue','orange','green','red','purple','brown','pink','gray','olive','cyan']

    for i in range(dataNum):
        # ax[i].scatter(ddist[i],marker=marker_arry[i])
        


        if(i<dataNum//2):
            ax[i,0].stem(ddist[i][:50],marker=marker_arry[i],color=color_array[i],label=label_arry[i])
            ax[i,0].legend()
            ax[i,0].get_xaxis().set_visible(False)
        else:
            ax[i-dataNum//2,1].stem(ddist[i][:50],marker=marker_arry[i],color=color_array[i],label=label_arry[i])
            ax[i-dataNum//2,1].legend()
            ax[i-dataNum//2,1].get_xaxis().set_visible(False)

    ax[dataNum//2-1,0].get_xaxis().set_visible(True)
    ax[dataNum//2-1,1].get_xaxis().set_visible(True)


    fig.suptitle("Robust Soliton Degree Distribution with K=256 c=0.1")
    plt.show()

def main():
    K =256#512#32#256
    dataSum=[]

    # delta_array =[20   5   0.5   0.1   0.05   0.01   0.005   0.001   ]
    with open('./delta_array.txt','r') as fl:
        delta_array=[int(x) if float(x)>=1.0 else float(x)  for x in fl.read().split()]
    # print(delta_array)

    for delta in delta_array:
        with open('RSD_K'+str(K)+'delta'+str(delta)+'.txt','r') as fl:
        # with open('simDDK'+str(K)+'delta'+str(item)+'.txt','r') as fl:
            ddist=[float(x) for x in fl.read().split()]
            dataSum.append(ddist)
            
        # print ("K"+str(K)+" ddist length",len(ddist))


    show_possibility(dataSum,delta_array)
    # show_num(ddist,delta_array,packet_n):


  

if __name__ == "__main__":
    main()