#! /usr/bin/env python3
# encoding: utf-8

import matplotlib.pyplot as plt
import math
import numpy as np

def show(data):
    dataNum=len(data)
    fig,ax = plt.subplots(dataNum,1)

    label_arry = ['delta=10','delta=0.1','delta=0.0001','delta=0.00001']
    marker_arry = ['*','o','P','x','+','1','v','2','3','4','5','8','9','.']

    for i in range(dataNum):
        # ax[i].scatter(data[i],marker=marker_arry[i])
        ax[i].plot(data[i][:50],marker=marker_arry[i])


    fig.suptitle("result")
    plt.show()

def main():
    K =256#512#32#256
    dataSum1=[]
    dataSum2=[]
    

    filename1='best_dd_K32'
    filename2='best_dd_K64'
    filename3='best_dd_K128'
    simDegreeK256M32m8 = 'simDegreeK256M32m8'
    filename_array =[filename1,filename2,filename3,simDegreeK256M32m8]

    fileNumber = len(filename_array)
    axNumber = fileNumber

    data_array=[]
    average_array=[]
    for filename in filename_array:
        average=0
        with open(filename+'.txt','r') as fl:
            data=[float(x) for x in fl.read().split()]
        
        data_array.append(data)

        for i in range(len(data)):
            average+=i*data[i]
        
        average_array.append(average)
        print("average name = ", average)

    ############################################
    average=0
    with open('simDegreeK256M32m8.txt','r') as fl:
        data=[float(x) for x in fl.read().split()]
        
    data_array.append(data)

    for i in range(len(data)):
        average+=i*data[i]
    
    average_array.append(average)
    print("simDegreeK256M32m8 = ", average)

    fig,ax=plt.subplots(axNumber,1)
    for i in range(axNumber):
        ax[i].stem(data_array[i],label=filename_array[i])
        # ax[i].
        ax[i].legend()
    
    plt.show()

    # with open('simDegreeK'+str(K)+'M32m8.txt','r') as fl:
    # # with open('simDegreeK'+str(K)+'M32m8_loop100.txt','r') as fl:
    # # with open('simDegreeK'+str(K)+'M32m8_loop'+str(loop)+'_bp.txt','r') as fl:

    #     data=[float(x) for x in fl.read().split()]
    #     print ("K"+str(K)+" data length",len(data))

    #     local = []
    #     for i in range(len(data)): 
    #         if data[i] !=  0.0000000e+00:

    #             # print(data[i])
    #             local.append(i)
    #     print("local Non Zero K",str(K),":",local)
    #     average_degree=0
    #     for i in range(len(local)):
    #         # average_degree += (i+1)*float(data[local[i]])
    #         average_degree += local[i]*data[local[i]]

    #     print("average degree:",average_degree)
    # dataSum1.append(data)
    # dataSum2.append(data)

    # # plt.plot(data)
    # # plt.show()

    # ####################################################################
    # for loop in [1,10,100,1000]:
    #     # with open('simDegreeK'+str(K)+'M32m8_reloop'+str(loop)+'.txt','r') as fl:
    #     # with open('simDegreeK'+str(K)+'M32m8_loop'+str(loop)+'_bp.txt','r') as fl:
    #     with open('sim_reloop'+str(loop)+'.txt','r') as fl:


    #         data=[float(x) for x in fl.read().split()]
    #         dataSum1.append(data)
    #     print ("K"+str(K)+" data length",len(data))

    #     local = []
    #     for i in range(len(data)): 
    #         if data[i] !=  0.0000000e+00:

    #             # print(data[i])
    #             local.append(i)
    #     print("local Non Zero K",str(K)," loop ",str(loop),":",local)
    #     average_degree=0
    #     for i in range(len(local)):
    #         # average_degree += (i+1)*float(data[local[i]])
    #         average_degree += local[i]*data[local[i]]

    #     print("average degree:",average_degree)

    # # show(dataSum1)
    # ####################################################################

    # for loop in [1,10,100,1000]:
    #     with open('sim_reloop'+str(loop)+'.txt','r') as fl:
    #     # with open('simDegreeK'+str(K)+'M32m8_loop'+str(loop)+'_bp.txt','r') as fl:

    #         data=[float(x) for x in fl.read().split()]
    #         dataSum2.append(data)

    #     print ("K"+str(K)+" data length",len(data))

    #     local = []
    #     for i in range(len(data)): 
    #         if data[i] !=  0.0000000e+00:

    #             # print(data[i])
    #             local.append(i)
    #     print("local Non Zero K",str(K)," loop ",str(loop),":",local)
    #     average_degree=0
    #     for i in range(len(local)):
    #         # average_degree += (i+1)*float(data[local[i]])
    #         average_degree += local[i]*data[local[i]]

    #     print("average degree:",average_degree)
    # show(dataSum2)




if __name__ == "__main__":
    main()