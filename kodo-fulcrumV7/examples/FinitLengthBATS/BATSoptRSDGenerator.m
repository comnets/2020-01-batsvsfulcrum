addpath expmv

clear


M = 32 ;
q = 256 ;
formatSpec = '%f';
fileID = fopen('simRankDistM32m8.txt','r');
h = fscanf(fileID,formatSpec);
h = h';
fclose(fileID);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% control padel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
go_test = false;
go_module = 'inac';%'BP'; % 'inac'
run_loop = 100;%25;  % 100
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% test start
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Run Finite-Length Inactivation Optimization!');




%for K = [32 64 128 1024]
%for K = [1024 512 256 128 64 32]  
for K = [512 256]
    disp(['Now run K = ' num2str(K)])
    %asy = BATSAsymp(M,K,q,h);
    flopt = BATSFLopt_RSD(M,K,q,h);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Inactivation 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % K=32
    % 'simDDK32delta0.1.txt'
    inputfilename=['V1simDDK' num2str(K) 'delta0.1.txt'];
    
    
    fileID = fopen(inputfilename,'r');
    dd_asy_rsd = fscanf(fileID,formatSpec);
    dd_asy_rsd = dd_asy_rsd';
    fclose(fileID);
    
    %opDDK32delta0.1_25.txt
    outputfilename_25=['V1opDDK' num2str(K) 'delta0.1_25.txt'];
    outputfilename_50=['V1opDDK' num2str(K) 'delta0.1_50.txt'];
    outputfilename_100=['V1opDDK' num2str(K) 'delta0.1_100.txt'];
    
    % [best_dd,best_c,best_delta] = flopt.robustSolitonOpt(25,100,'inac');
    % [ddout, numdeg, iter] = obj.runOpt_Inac(objfunc,ddin,numiter);
    % [ddout, numdeg, iter] = obj.runOpt_Inac(objfunc,ddin,numiter);
    % [dd_out, numdeg, iter] = ddOpt(n,ddin,numiter,flag)
    [dd_25, numdeg, iter] = flopt.ddOpt(25,dd_asy_rsd,run_loop,'inac');
    
%     if isequal(go_module,'inac')
%         [dd_25, dd_25_diff] = flopt.ddOpt(25,dd_asy_rsd,run_loop,'inac'); 
%     elseif isequal(go_module,'BP')
%         [dd_25, dd_25_diff] = flopt.ddOpt(25,dd_asy_rsd,run_loop,'BP'); 
%     else
%         disp('Please give inac or BP');
%     end
%     save(outputfilename_25,'dd_25', '-ASCII','-append');

    save(['best_dd_K' num2str(K) '.txt'],'dd_25', '-ASCII','-append');
    
    if go_test==true
        if isequal(go_module,'inac')
            [dd_50, dd_25_diff] = flopt.ddOpt(50,dd_asy_rsd,run_loop,'inac'); 
        elseif isequal(go_module,'BP')
            [dd_50, dd_25_diff] = flopt.ddOpt(50,dd_asy_rsd,run_loop,'BP'); 
        else
            disp('Please give inac or BP');
        end
            
        save(outputfilename_50,'dd_50', '-ASCII','-append');
        
        if isequal(go_module,'inac')
            [dd_100, dd_25_diff] = flopt.ddOpt(100,dd_asy_rsd,run_loop,'inac');
        elseif isequal(go_module,'BP')
            [dd_100, dd_25_diff] = flopt.ddOpt(100,dd_asy_rsd,run_loop,'BP'); 
        else
            disp('Please give inac or BP');
        end
        save(outputfilename_100,'dd_100', '-ASCII','-append');
    end
 end

