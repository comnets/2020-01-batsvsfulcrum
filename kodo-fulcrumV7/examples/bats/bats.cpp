#include <cstdlib>
#include "include/Bench_BATS.hpp"
#include <string>

using namespace std;
int main(int argc, char **argv){



    uint32_t packetNum_K; // = 1600;
    uint32_t packetSize_T = 1600;
    uint32_t recoder_Num = 3;
    uint32_t iterationNum = 100;
    uint32_t fifi_size = 8;
    // uint32_t batchSize = 32;
    uint32_t batchSize = 16;//32; //16
    double erasure = 0;
    std::string operation = "XX";


    switch(argc) {
        case 1:
            // batchSize = 32; // default 32.  16, 32, 64
            packetNum_K = 1600; // default 1600
            // iterationNum = 10; // default 40
            break;

        case 2:
            // batchSize = 32; // default 32.  16, 32, 64
            packetNum_K = atoi(argv[1]);//1600; // default 1600
            // iterationNum = 10; // default 40
            break;
        case 3:
            // batchSize = 32; // default 32.  16, 32, 64
            packetNum_K = atoi(argv[1]);//1600; // default 1600
            iterationNum = atoi(argv[2]);
            // batchSize = atoi(argv[2]);
            // iterationNum = 10; // default 40
            break;   
        case 4:
            packetNum_K = atoi(argv[1]);
            erasure = atoi(argv[2]);
            iterationNum = atof(argv[3]);
            break;
        case 5:
            packetNum_K = atoi(argv[1]);
            erasure = atof(argv[2]);
            iterationNum = atoi(argv[3]);
            operation = argv[4];
            break;
        default:
            cout << "simbats M K numIteration" << endl;
            return 0;
    }

    
    
    // Benchmark_BATS bats;
    /* Benchmark_BATS(int M, int q, int K, int T): */
    std::cout << "Input Operation is "<< operation << ", sieze is " << operation.size() << "\n";

    Benchmark_BATS bats(iterationNum,batchSize,fifi_size,packetNum_K,packetSize_T,erasure,operation);
    bats.test();

    return 0;
}