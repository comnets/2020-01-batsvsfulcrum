/*
 Copyright (C) 2014 Shenghao Yang
 Copyright (C) 2019 Yicong Su

 This file is part of SimBATS.
 
 SimBATS is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 SimBATS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with SimBATS.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once 

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <sys/times.h>
#include <vector>

// #include "Utilities.h"
// #include "BatchEnc.h"
// #include "BatchDec.h"
// //#include "NCCoder.h"
// #include "FiniteField.h"
// #include "simRecoder.h"

// #include "BatchEnc.cpp"
// #include "BatchDec.cpp"
// #include "FiniteField.cpp"


// #include "include/Utilities.h"
// #include "include/BatchEnc.h"
// #include "include/BatchDec.h"
// //#include "NCCoder.h"
// #include "include/FiniteField.h"

// #include "include/simRecoder.h"
#include "BatsSimulator.hpp"

#include "TimeCounter.hpp"

// using namespace std;

class Benchmark_BATS
{
    private:
        // set parameters
        int batchSize;
        int gf_order; // = 8; // 1, 2, 4, 8
        int packetNum;
        int packetSize; //1024; //In Bytes
        //int packetSizeInSymbol = packetSize * SymbolSize / gf_order;

        int iterationNum;
        //const float decRatio = 0.99;
        double erasure;
        string operation;

    public:
        Benchmark_BATS(){
            // set parameters
            batchSize = 32; // default 32.  16, 32, 64
            packetNum = 1600; // default 1600
            iterationNum = 100; // default 40

            gf_order = 8; // 1, 2, 4, 8
            packetSize = 1600; //1024; //In Bytes
            erasure = 0;
            operation = "XX";
            //int packetSizeInSymbol = packetSize * SymbolSize / gf_order;
            //const float decRatio = 0.99;
        }
        Benchmark_BATS(int itNum, int M, int q, int K, int T, double erasure,string operation):
                        iterationNum(itNum),batchSize(M),gf_order(q),packetNum(K), 
                        packetSize(T), erasure(erasure),operation(operation){
            // set parameters
            // iterationNum = 10; // default 40
            //int packetSizeInSymbol = packetSize * SymbolSize / gf_order;
            //const float decRatio = 0.99;
        }
        ~Benchmark_BATS(){}

        void test(){
            BATSimulator sim(batchSize, gf_order, packetNum, packetSize, erasure, operation);

            std::vector<double> en_throughput_buffer;
            std::vector<double> re_throughput_buffer;
            std::vector<double> de_throughput_buffer;

            std::cout << "Simulation starts with " << "M = " 
                 << batchSize << ", q = 2^" << gf_order 
                 << ", K = " << packetNum << ", T = " 
                 << packetSize << endl;

            int iter = 0;

            int errIdx[iterationNum];
            int nSucc = 0;
            int nErrIdx = 0;
            float totalTrans = 0.0;
            // TimeUsed timeUsed;
            TimeCounter tCounter;
            // DegeeBuffer degreeBuffer;

            DecoderStatus ds(batchSize);
            double accuRankDist[batchSize + 1]={0};

            // double accuRankDist[batchSize + 1];
            // for (int i = 0; i < batchSize + 1; i++) {
            //     accuRankDist[i] = 0;
            // }

            ofstream output,output2;
            
            //output the result
            // time_t t = time(0);
            // struct tm * now = localtime(&t);
            // stringstream iss;
            // iss << "simK" << packetNum << "M" 
            //               << batchSize << "m" << gf_order << "(" << now->tm_mon + 1 
            //               << now->tm_mday << now->tm_hour 
            //               << now->tm_min << now->tm_sec << ").txt";

            stringstream iss, iss2;
            if(operation=="ISD")
                iss << "ISD_K" << packetNum << "M" << batchSize << "loss" << erasure<<"_run"<<iterationNum <<".csv";

            else if(operation=="RSD")
                iss << "RSD_K" << packetNum << "M" << batchSize << "loss" << erasure<<"_run"<<iterationNum <<".csv";

            else if(operation=="Asy0.1")// Asy
                iss << "Asy0.1_K" << packetNum << "M" << batchSize << "loss" << erasure<<"_run"<<iterationNum <<".csv";
            else if(operation=="Asy0.1_OP")// Asy
                iss << "Asy0.1_OP_K" << packetNum << "M" << batchSize << "loss" << erasure<<"_run"<<iterationNum <<".csv";
            else if(operation=="Asy0.01")// Asy
                iss << "Asy0.01_K" << packetNum << "M" << batchSize << "loss" << erasure<<"_run"<<iterationNum <<".csv";
            else if(operation=="Asy0.01_OP")// Asy
                iss << "Asy0.01_OP_K" << packetNum << "M" << batchSize << "loss" << erasure<<"_run"<<iterationNum <<".csv";

            else
                std::cout << "the Operation is wrong" << std::endl;

            // iss2 << "Degree.py";

            output.open(iss.str().c_str());
            // output2.open(iss2.str().c_str());

            // output2 << "[";
            
            double erasure1 = 0.2;
            double erasure2 = 0.1;

            double encoderThrough=0;
            double recoderThrough=0;
            double decoderThrough=0;
            
            double channelLossRate = 0;
            double DecoderRate = 0;

            double enThroughArray[iterationNum]; 
            double reThroughArray[iterationNum]; 
            double deThroughArray[iterationNum];

            double ChannelLossArray[iterationNum];
            double DecoderRateArray[iterationNum]; 

            double facktorM=1000;

             
            output<< "nEncBatch,"
                    << "nTrans,"
                    << "nReceive,"
                    // << "nTranspacket,"

                    << "encoderThrough,"
                    << "decoderThrough,"
                    << "DecoderRate,"
                    << "channelLossRate,"

                    << "TotalEncTime,"
                    << "TotalDecTime,"
                    << "TotalCodingTime,"
                    << "nSave,"
                    << "nInact,"
                    << "nError,"
                  
                    << "Aver_rank,"
                    << "Aver_rank_Over_BatchSize\n";




            while (iter < iterationNum) {
                std::cout << "===== " << iter << " =====" << endl;
                // sim.runOnce(timeUsed, ds);
                sim.runOnce(tCounter, ds);
                // sim.runOnce(tCounter, ds, degreeBuffer);


                std::cout << "nEncoded Batch:" << ds.nEncBatch << ", packetNum / batchSize ("<< packetNum<< "/"<< batchSize << ") = " << packetNum/batchSize
                    << ", nTrans Packets :"<<ds.nTrans << ", loss:" << erasure1 << ", nRec Packets:"<<ds.nReceive <<endl;
                std::cout << "(Decoded- ds.nError)/Total: " 
                    << packetNum - ds.nError << "/" << packetNum << " With " << ds.nTrans << " packets transmitted" << endl;

                // uint32_t factM = 1000000; // MB
                // double fact = 1000;
                encoderThrough = ds.nTrans * packetSize *facktorM/ (double) (tCounter.encoding.time());
                // decoderThrough = (double) (packetNum - ds.nError) * packetSize*fact/ tCounter.encoding.time();
                decoderThrough = ds.nReceive * packetSize *facktorM/ (double) (tCounter.decoding.time());
                // std::cout << "Encoding throuth = " << encoderThrough  <<" MB/s" << endl;
                // std::cout << "DEcoding throuth = " << decoderThrough  <<" MB/s" << endl;
              

                channelLossRate = (ds.nTrans-ds.nReceive)/(double)ds.nTrans;
                std::cout << "(ds.nTrans-ds.nRecpacket)/(double)ds.nTrans=" <<(ds.nTrans-ds.nReceive) <<"/"<<(double)ds.nTrans <<"\n";
                DecoderRate = (packetNum - ds.nError) / (double)ds.nReceive;


                enThroughArray[iter]=encoderThrough;
                reThroughArray[iter]=recoderThrough; 
                deThroughArray[iter]=decoderThrough; 

                ChannelLossArray[iter] = channelLossRate;
                DecoderRateArray[iter] = DecoderRate;
                iter++;

                
                std::cout << "DecoderRate= " << DecoderRate << endl;

                double Erk = 0.0;

                if (ds.nError == 0) {
                    nSucc++;
                    totalTrans += ds.nTrans;
                } else {
                    errIdx[nErrIdx++] = iter-1;
                }

                // rank distribution
                // 
                for (int i = 0; i <= batchSize; i++) {
                    // output << ds.rankdist[i] << " ";
                    std::cout << ds.rankdist[i] << " ";
                    Erk += i * ds.rankdist[i];
                    accuRankDist[i] += ds.rankdist[i];
                }
                // output<< "nEncBatch,"
                //     << "nTrans,"
                //     << "nReceive,"
                //     // << "nTranspacket,"

                //     << "encoderThrough,"
                //     << "decoderThrough,"
                //     << "DecoderRate,"
                //     << "channelLossRate,"

                //     << "TotalEncTime,"
                //     << "TotalDecTime,"
                //     << "TotalCodingTime,"
                //     << "nSave,"
                //     << "nInact,"
                //     << "nError,"
                  
                //     << "Aver_rank,"
                //     << "Aver_rank_Over_BatchSize\n";



                 
                 output << ds.nEncBatch << "," 
                        << ds.nTrans << "," 
                        << ds.nReceive << "," 
                        // << ds.nTranspacket << ","

                        << encoderThrough << ","
                        << decoderThrough << ","
                        << DecoderRate << ","
                        << channelLossRate << ","

                        << tCounter.encoding.time()/facktorM << ","
                        << tCounter.decoding.time()/facktorM << ","
                        << tCounter.coding.time()/facktorM << ","
                        << ds.nSave << "," 
                        << ds.nInact << "," 
                        << ds.nError << ","

                        << Erk << ","
                        << Erk / (float) batchSize << "\n";



                std::cout << endl;
                std::cout << "E[rank(H)] = " << Erk << "(" << Erk / (float) batchSize << ")" << endl;
            }

            output.close();

            std::cout << "======== END =======" << endl;
            std::cout << "Simulation ends with " << nSucc << " succeeds out of " << iter << " runs" << endl;

            std::cout << "Average Rank Distribution: ";

            double totalRank = 0;
            double sumRank = 0;
            for(int i = 0; i <= batchSize; i++) {
                sumRank += accuRankDist[i];
            }
            for(int i = 0; i <= batchSize; i++) {
                accuRankDist[i] /= sumRank;
                totalRank += i * accuRankDist[i];
                std::cout << accuRankDist[i] << " ";
            }
            std::cout << endl;

            std::cout << "Average rate of succeeded runs: "  << endl
                << "nSucc * packetNum / (float)totalTrans = " << nSucc << " * " << packetNum << "/ " << (float)totalTrans << " = " 
                <<  nSucc * packetNum / (float)totalTrans  << endl
                << "Vs average rank: " << "totalRank " << " / " << "(float) batchSize = " << totalRank << "/" << (float) batchSize << " = "
                << totalRank / (float) batchSize << endl;

            std::cout << "Error iterations: ";
            for (int i = 0; i < nErrIdx; i++) {
                std::cout << errIdx[i] << " ";
            }
            std::cout << endl;

            
            /*
            / result of average 
            */
           double EnThAverage=0;
           double ReThAverage=0;
           double DeThAverage=0;
           double ChannelLossAv=0;
           double DecodeRateAv=0;
           for(int i=0; i<iterationNum;i++)
           {
               EnThAverage += enThroughArray[i]; 
               ReThAverage += reThroughArray[i]; 
               DeThAverage += deThroughArray[i]; 

               ChannelLossAv += ChannelLossArray[i];
               DecodeRateAv  += DecoderRateArray[i];
           }
            EnThAverage = (double) EnThAverage/iterationNum;
            ReThAverage = (double) ReThAverage/iterationNum;
            DeThAverage = (double) DeThAverage/iterationNum;
         
            ChannelLossAv = (double) ChannelLossAv/iterationNum;
            DecodeRateAv  = (double) DecodeRateAv/iterationNum;

            std::cout << "AverEncThroughtput "  << EnThAverage << " MB/s " 
                        << "AverDecThroughtput " <<  DeThAverage << " MB/s "  
                        << "AverChannel Loss: " << ChannelLossAv << " "
                        << "DecodeRateAv Loss: " << DecodeRateAv
                        << std::endl;

            
        }
    
};
