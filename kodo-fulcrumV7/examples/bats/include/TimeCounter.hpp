#pragma once
#include <cstdlib>
#include <chrono>
#include <cassert>
#include <sys/times.h>
#include <ctime>
#include <iostream>

class TimeCounter{
    using time_chrono = std::chrono::high_resolution_clock;
    private:
        struct TimeContainer
        {
            private:
            
                time_chrono::time_point start_t, stop_t;
                unsigned long long period = 0; // long int
                double factorG = 1000000000.0; // s
                double factorM = 1000000.0; // ms
                double factorU = 1000.0; // us
                double factorN = 1.0; // ns

            public:
                void start(){
                    start_t = time_chrono::now();
                }

                void stop(){
                    stop_t = time_chrono::now();

                    assert(start_t <= stop_t);
                    period += std::chrono::duration_cast<std::chrono::nanoseconds>(stop_t - start_t).count();
                }

                void clear(){
                    period = 0;
                }
           
                double time(){
                    // return (double) period/factorU;
                    return (double) period;

                }
            

                
        };
        
    public:
        TimeContainer encoding;
        TimeContainer decoding;
        TimeContainer recoding;

        TimeContainer coding;
        
        unsigned long long total_coding_time(){
            return encoding.time() + decoding.time() + recoding.time();
        }
        
        void clear(){
            encoding.clear();
            decoding.clear();
            recoding.clear();
        }
        
};




// class TimeUsed{
//     private:
//         struct Interval{
//             private:
//                 struct tms start_t, end_t;  // tms: long int (at least 32 bits)
//                 long s, e;
//                 long t;
//             public:
//                 void clear(){
//                     t = 0;
//                 }

//                 void start(){
//                     s = times(&start_t);
//                 }

//                 void end(){
//                     e = times(&end_t);
//                     t += e - s;
//                 }

//                 double time(){
//                     return (double) t/60; //sysconf(_SC_CLK_TCK);
//                 }
//         };
//         // double en_de_time;

//     public:
//         Interval encoding;
//         Interval decoding;
//         Interval nccoding;
//         Interval recoding;
//         Interval coding;
//     public:
//         void clear(){
//             encoding.clear();
//             decoding.clear();
//             recoding.clear();
//             nccoding.clear();
//             coding.clear();
//         }
//         double get_en_de_time(){
//             return coding.time();
//         }
// };

class DecoderStatus{
public:
    // int nTrans;
    // int nReceive;
    // int nSave;
    
    // int nError;
    // int nInact;

    uint32_t nTrans;
    uint32_t nReceive;
    uint32_t nSave;
    uint32_t nRecpacket;
    
    uint32_t nError;
    uint32_t nInact;
    uint32_t nEncBatch;
    uint32_t nTranspacket;

    // uint32_t nDegree=0;
    double * rankdist;

    DecoderStatus(int M){
        rankdist = new double[M+1];
    }
    ~DecoderStatus(){
        delete [] rankdist;
    }
};

class DegeeBuffer{
    public:
        vector<uint16> DegreeArray;
        double AverDegree;
        uint16 SumDegree;
        DegeeBuffer(){
            AverDegree=0;
            SumDegree=0;
        }
        ~DegeeBuffer(){
        }
        // void addDegree(uint32_t deg){
        //     SumDegree+=deg;
        // }
        // void getAverDegree(){
        //     uint16 temp=0;
        //     for(auto item:DegreeArray)
        //         temp+=item;
        //     AverDegree= (double)temp/DegreeArray.size();
        // }
        // void printDegree(){
        //     std::cout <<"Degree:=[";
        //     for(uint16 i=0;i<DegreeArray.size();i++){
        //         std::cout << DegreeArray[i];
        //         if(i==DegreeArray.size()-1)
        //             std::cout<<"]" <<std::endl;
        //         else 
        //             std::cout <<",";
        //     }
        // }
        // void printAverDeg(){
        //     getAverDegree();
        //     std::cout << "Average Degree : "<< AverDegree << std::endl;
        // }
        // void printSortDeg(){

        // }
        void clear(){
            DegreeArray.clear();
            AverDegree=0;
            SumDegree=0;
        }
};