#pragma once

#include <fstream>
#include <iostream>
#include <sstream>

#include <vector>

#include "Utilities.h"
#include "BatchEnc.h"
#include "BatchDec.h"
// //#include "NCCoder.h"
#include "FiniteField.h"
#include "simRecoder.h"

#include "TimeCounter.hpp"

#include "BatsSimulator.hpp"

// Parameters
// code parameters
#define BATCH_SIZE 16 //M
#define PACKET_LEN 2// T
#define GFORDER 8   //m
#define PACKET_NO 64000 //K
#define DESIGN_DEC_RATIO 0.96 //1-eta
#define DEC_RATIO DESIGN_DEC_RATIO // for control only
#define ERASURE_R 0.2   //e
#define ITERATION_NO 1000
#define CHANEL_TYPE 1 // 0 tri 1 tri one 2 sq (Change channel in code!!!)


class BATSimulator {
private:
    int batchSize;
    int gf_order;
    int packetNum;
    int packetSize;

    int nEncLimit;
    int packetSizeWithHeader;
    int packetSizeWithHeaderAndId;

    double degreeDist[MAX_DEGREE];
    int D;

	double* rankDist;

    MTRand *psrand;
    SymbolType* input;
    SymbolType* output;

    BatsEncoder *encoder;
    BatsDecoder *decoder;
    //NCCoder *nccoder;
    SimRecoder *simcoder;

    // add index
    double erasure;
    string operation;

    
    

public:
    BATSimulator(int M, int m, int K, int T, double erasure, string operation) : 
        batchSize(M), gf_order(m), packetNum(K), packetSize(T), erasure(erasure), operation(operation) {

        nEncLimit = packetNum / batchSize * 30; //nEnc < nEncLimit && !encEnd
        packetSizeWithHeader = packetSize + batchSize * gf_order / 8;
        packetSizeWithHeaderAndId = packetSizeWithHeader + sizeof(KeyType);

        // Get degree distribution according to the channel
        getDegDist();

		// Get rank distribution
		getRankDist();

		//ff_init();//Tom: Added on interface change
        
        FF.setOrder(gf_order);

        psrand = new MTRand(); // 5344 no // 3544 all

        input = new SymbolType[packetNum * packetSize];

        output = new SymbolType[packetNum * packetSize];

        // std::cout <<".......Input Packet...........\n";
        for (int i = 0; i < packetNum; i++) {
            // std::cout << "Input Packet %d:"<< i<<"\n";
            for (int j = 0; j < packetSize; j++) {
                input[j + i * packetSize] = psrand->randInt(255);
                // input[j + i * packetSize] = 111;
                // std::cout << int(input[j + i * packetSize]) << " ";

                output[j + i * packetSize] = 255;
            }
        }

        encoder = NULL;
        decoder = NULL;
        //nccoder = NULL;
        simcoder = NULL;
    }

    ~BATSimulator() {
        delete psrand;
        delete [] input;
        delete [] output;
        delete [] rankDist;
        clearNetwork();
    }

private:
    void getDegDist() {
        stringstream iss;

        // (ISD) ISD_K.txt
        if(operation=="ISD")
            iss << "ISD_K" << packetNum << ".txt";

        else if (operation=="RSD")
        // (RSD) RSD_K16_delta0.1.txt
            iss << "RSD_K" << packetNum << "_delta0.1.txt";

        else if (operation=="Asy0.1") // (Asy) ddK16M16_paper_0.01
            iss << "ddK"<< packetNum <<"M16_paper_0.1.txt";

        else if (operation=="Asy0.1_OP") // (Asy) ddK16M16_paper_0.01
            iss << "ddK"<< packetNum <<"M16_paper_3.txt";

        else if (operation=="Asy0.01") // (Asy) ddK16M16_paper_0.01
            iss << "ddK"<< packetNum <<"M16_paper_0.01.txt";

        else if (operation=="Asy0.01_OP") // (Asy) ddK16M16_paper_0.01
            iss << "ddK"<< packetNum <<"M16_paper_2.txt";
        else
            std::cout << "The Error is occuring!" << std::endl;

        ifstream filestr;

        cout << "file name is  "<< iss.str() << "" << endl;
        filestr.open(iss.str().c_str());
        if (!filestr) {
            cout << "cannot get degree distribution. File name:" << iss.str().c_str() << "\n";
            return;
        }

        // the degree file start with degree 1.
        // degree 0 has probability 0.
        degreeDist[0] = 0.0;

        double x;
        D = 1;
        while (filestr >> x && D < MAX_DEGREE) {
            degreeDist[D] = x;
            D++;
        }

        int j;
        for (int i = D; i < MAX_DEGREE; i++) {
            degreeDist[i] = 0;
            j = i; // get the number of i
        }

        filestr.close();

        // print out the degree
        cout << "last degree number of D: "<< j << endl;
    }

	void getRankDist() {
        rankDist = new double[batchSize+1];

		for (int i = 0; i < batchSize + 1; i++)
			rankDist[i] = 0;

        stringstream iss;

        iss << "simRankDistM" << batchSize << "m" << gf_order << ".txt";

		ifstream filestr;
		filestr.open(iss.str().c_str());
		if (!filestr) {
			cout << "cannot get simulation rank distribution. File name:" << iss.str().c_str() << endl;
			return;
		}

		double x, sum;
		sum = 0;
		for (int i = 0; i < batchSize + 1; i++) {
			if (filestr >> x) {
				rankDist[i] = x;
				sum += x;
			} else {
				cout << "Warning: file ended with only " << i << " entries out of " << batchSize + 1 << " needed." << endl;
				filestr.close();
				return;
			}
		}
		filestr.close();

		if (sum < 1.0)
			cout << "Warning: Rank distribution does not sum to 1 (" << sum << ")" << endl;
	}

    void clearNetwork(){
        if (encoder != NULL)
            delete encoder;
        if (decoder != NULL)
            delete decoder;
        //if (nccoder != NULL)
        //    delete nccoder;
        if (simcoder != NULL)
			delete simcoder;
    }

    void initNetwork() {
        // Initialize encoder
        clearNetwork();

        encoder = new BatsEncoder(batchSize, input, packetNum, packetSize);
        //encoder = new BatsEncoder(batchSize, packetNum, packetSize, input);//Tom: interface change
		encoder->setDegreeDist(degreeDist, D);

        //bool ch = encoder.verifyCheckPkg();

        // Initialize decoder
        decoder = new BatsDecoder(batchSize, output, packetNum, packetSize);
		//decoder = new BatsDecoder(batchSize, packetNum, packetSize, output);//Tom: interface change
        decoder->setDegreeDist(degreeDist, D);

        // Initialize nccoder
        //nccoder = new NCCoder(batchSize, packetSizeWithHeader);
		//nccoder = new NCCoder(batchSize, packetSize);

		// Initialize simcoder
		simcoder = new SimRecoder(batchSize, packetSizeWithHeader, (1 << gf_order) - 1, rankDist, psrand);
    }

public:
    // TimeCounter tCounter;
    void runOnce(TimeCounter& tCounter, DecoderStatus& ds) {
    // void runOnce(TimeCounter& tCounter, DecoderStatus& ds, DegeeBuffer& db) {

        initNetwork();

        run(tCounter, ds);
        // run(tCounter, ds, db);

        // get Rank Dist from rank Dist
        decoder->rankDist(ds.rankdist);


        // input and output compare check
        // know the packet which is lost
        ds.nError = 0;
        for (int i = 0; i < packetNum; i++) {
            for (int j = 0; j < packetSize; j++) {
                if (input[j + i * packetSize] != output[j + i * packetSize]) {
                    ds.nError++;
                    break;
                }
                // std::cout << "Input Packet %d: %d",i,input[j + i * packetSize] << "\n";
                // std::cout << "its input \n";
            }
        }
    }
    
    int enc_index = 0;
    int rec_index = 0;
    int dec_index = 0;
    int nEncBatch = 0;
    int nRecpacket = 0;
    int nTranspacket = 0;

    void run(TimeCounter& tCounter, DecoderStatus& ds) {
    // void run(TimeCounter& tCounter, DecoderStatus& ds,  DegeeBuffer& db) {

        SymbolType** batch = mallocMat<SymbolType>(batchSize, packetSizeWithHeaderAndId);
        SymbolType** batchWithoutId = (SymbolType**) malloc(batchSize * sizeof (SymbolType*));

		//Recoding batch
        SymbolType** recBatch = mallocMat<SymbolType>(batchSize, packetSizeWithHeaderAndId);
        SymbolType** recBatchWithoutId = (SymbolType**) malloc(batchSize * sizeof (SymbolType*));

        // move to the addresss of batch data
        for (int i = 0; i < batchSize; i++) {
            batchWithoutId[i] = batch[i] + sizeof(KeyType);
			recBatchWithoutId[i] = recBatch[i] + sizeof(KeyType);
        }

        //SymbolType* relayOut = new SymbolType[packetSizeWithHeaderAndId];

        KeyType id;

        // int nEnc = 0; // 
        int encEnd = false;

		int recCnt;//Number of recoded packets in current batch
        // double erasure1 = 0.1;
        // double erasure2 = 0.1;
      
        tCounter.clear();
        enc_index = 0;
        rec_index = 0;
        dec_index = 0;
        nEncBatch = 0;
        nTranspacket = 0;
        // vector<uint16> DegreeArray;
        // db.clear();
        // nEncLimit = packetNum / batchSize * 30
        tCounter.coding.start();

        while (nEncBatch < nEncLimit && !encEnd) {
            
            // enc_index ++;
            // timeUsed.encoding.start();
            /* Simulation of Encoder */ 
            // cout<< "Degree of this Batch:\n" <<"["<<endl;
            
            tCounter.encoding.start();
            id = encoder->genBatch(batchWithoutId);
            // id = encoder->genBatch(batchWithoutId,db);

            for (int i = 0; i < batchSize; i++) {
                saveIDInPacket(batch[i], &id);
            }
            tCounter.encoding.stop();


            // std::cout << "\n...........................................\n";
            // std::cout<<"\n"<<"("<<id<<").th Batch packets\n";
            // // std::cout<<"\nbatch: (pidegree:"<<piDegree <<")\n";
            // for(int i=0;i<batchSize;i++){
            //     for(int j=0;j<packetSizeWithHeaderAndId;j++)
            //             std::cout <<int (batch[i][j])<<" ";
            //     printf("\n");
            // }


            // rec_index ++;
			/* Simulation of recoding */
            tCounter.recoding.start();
			recCnt = simcoder->genBatch(recBatchWithoutId, batchWithoutId,erasure);
			for (int i = 0; i < batchSize; i++) {
				saveIDInPacket(recBatch[i], &id);
			}
            tCounter.recoding.stop();

            // std::cout << "\n...........................................\n";
            // std::cout<<"\n"<<"("<<id<<").th reBatch \n";
            // // std::cout<<"\nbatch: (pidegree:"<<piDegree <<")\n";
            // for(int i=0;i<batchSize;i++){
            //     for(int j=0;j<packetSizeWithHeaderAndId;j++)
            //             std::cout <<int (recBatch[i][j])<<" ";
            //     printf("\n");
            // }

            // // transmit output in batchSize separated packets
            
            std::srand (time(0)); // have different rand sequence

            // dec_index ++;
			/* Update method for decoding */
            
            tCounter.decoding.start();
            for (int i = 0; i < recCnt; i++)
            // for (int i = 0; i < batchSize; i++)
            {
                // nTranspacket++;
                //Tom: Changed interface for recieving packets and inact decoding
                //decoder->receivePacket(recBatch[i]);
                // 10% loss
                // if (((double) rand() / RAND_MAX) < erasure){
                //     continue;
                // }
                decoder->receivePacket(batch[i] + sizeof (KeyType), getIDFromPacket(batch[i]));
                

                // decoder->receivePacket(recBatch[i] + sizeof (KeyType), getIDFromPacket(recBatch[i]));


                /*inline bool complete(double decRatio){
                    return (nDecodedPkg>=packetNum * decRatio);
                    }
                */
                if (decoder->complete(1.0))
                {
                    encEnd = true;
                    break;
                }

            }
            tCounter.decoding.stop();

            nEncBatch++;
        }//end while
        tCounter.coding.stop();
        
        // db.printDegree();
        // db.printAverDeg();
        
        ds.nRecpacket = nRecpacket;
        ds.nEncBatch = nEncBatch;
        ds.nTrans = nEncBatch * batchSize;
        ds.nReceive = decoder->nRecPkg;
        ds.nSave = decoder->nSavedPkg;
        ds.nInact = decoder->nInactVar;

        freeMat(batch, batchSize);
        free(batchWithoutId);

		freeMat(recBatch, batchSize);
		free(recBatchWithoutId);
        //delete [] relayOut;
		//delete [] recodeBatch;
    }
};