#include <cstdlib>
#include "include/Bench_Fulcrum.hpp"


// using namespace std;

int main(int argc, char **argv){



    uint32_t packetNum_K = 1600; // = 1600;
    uint32_t packetSize_T = 1600;
    uint32_t recoder_Num = 3;
    uint32_t iterationNum = 1000;
    uint32_t fifi_size = 8;
    // uint32_t batchSize = 32;
    double   erasure = 0.2;

    switch(argc) {
        case 1:
            // batchSize = 32; // default 32.  16, 32, 64
            erasure = 0.0; // default 1600
            // iterationNum = 10; // default 40
            break;

        case 2:
            // batchSize = 32; // default 32.  16, 32, 64
            erasure = (double) atof(argv[1]);
            // iterationNum = 10; // default 40
            break;
        case 3:
            // batchSize = 32; // default 32.  16, 32, 64
            erasure = (double) atof(argv[1]);
            iterationNum = atoi(argv[2]);
            // batchSize = atoi(argv[2]);
            // iterationNum = 10; // default 40
            break;   
        // case 4:
        //     packetNum_K = atoi(argv[1]);
        //     batchSize = atoi(argv[2]);
        //     iterationNum = atoi(argv[3]);
        //     break;
        default:
            std::cout << "simbats M K numIteration" << std::endl;
            return 0;
    }

    Benchmark_Fulcrum fulcrum(recoder_Num,erasure,iterationNum);
    fulcrum.test();

    return 0;
}