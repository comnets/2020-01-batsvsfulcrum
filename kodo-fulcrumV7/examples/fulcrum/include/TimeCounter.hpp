/*
 * @Author: your name
 * @Date: 2019-11-25 13:28:53
 * @LastEditTime : 2020-01-04 04:54:47
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /BB_kodo-fulcrum_V7/examples/fulcrum/include/TimeCounter.hpp
 */
#pragma once
#include <cstdlib>
#include <chrono>
#include <cassert>
#include <sys/times.h>
#include <ctime>



class TimeCounter{
    using time_chrono = std::chrono::high_resolution_clock;
    private:
        struct TimeContainer
        {
            private:
            
                time_chrono::time_point start_t, stop_t;
                unsigned long long period = 0; // long int
                // double period = 0.0; // long int
                double factorG = 1000000000; // s
                double factorM = 1000000; // ms
                double factorU = 1000.0; // us
                double factorN = 1; // ns
            public:
                void start(){
                    start_t = time_chrono::now();
                }

                void stop(){
                    stop_t = time_chrono::now();

                    assert(start_t <= stop_t);
                    period += std::chrono::duration_cast<std::chrono::nanoseconds>(stop_t - start_t).count();
                }

                void clear(){
                    period = 0; // long int
                }

                double time(){
                    // return (double) period/factorU;
                    return (double) period;

                }
            

                
        };
        
    public:
        TimeContainer decoding;
        TimeContainer recoding;

        TimeContainer encoder;
        TimeContainer inner_decoder;
        TimeContainer outer_decoder;
        TimeContainer combi_decoder;
        TimeContainer RLNC_decoder;
        
        TimeContainer total_coding;

        
        
        
        void clear(){
            decoding.clear();
            recoding.clear();

            encoder.clear();
            total_coding.clear();
            inner_decoder.clear();
            outer_decoder.clear();
            combi_decoder.clear();
        }
        
};
