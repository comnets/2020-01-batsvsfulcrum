// Copyright Steinwurf ApS 2015.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include <algorithm>
#include <cstdint>
#include <ctime>
#include <fstream>
#include <iostream>
#include <vector>
#include <cstdio>
#include <vector>
#include <string>


#include <storage/storage.hpp>

#include <kodo_fulcrum/fulcrum_codes.hpp>
// // #include <kodo_rlnc/decoder.hpp>
// #include <kodo_rlnc/coders.hpp>
// // #include <kodo_rlnc/decoders.hpp>

// #include "kodo_rlnc/coders.hpp"

#include "TimeCounter.hpp"
/// @example encode_decode_fulcrum.cpp
///
/// Simple example showing how to encode and decode a block of data using
/// the fulcrum codec.
/// For a detailed description of the fulcrum codec see the following paper
/// on arxiv: http://arxiv.org/abs/1404.6620 by Lucani et. al.

class Benchmark_Fulcrum
{
    private:
            uint32_t symbols;// = 1600;
            uint32_t symbol_size; // = 1600; //1600 bytes
            uint32_t recoder_symbols; // = 3;
            std::vector<uint32_t> symbols_array; //[] = {16,32,64,128,256,512,1024};
            uint32_t sizeofsymb_Array; //= sizeof(symbols_array)/sizeof(symbols_array[0]);

            std::vector<uint32_t> expansion_array; // []; //[] = {1,2,3,4};
            uint32_t sizeofexpa_Array; // = sizeof(expansion_array)/sizeof(expansion_array[0]);

            // uint32_t symbols_array [] = {16,32};
            long long RUN_LOOP; // = 100;
            double erasure1;
    public:
            Benchmark_Fulcrum(){
                symbols = 1600;
                symbol_size  = 1600; //1600 bytes
                recoder_symbols = 3;
                
                for(auto item:{16,32,64,128,256,512,1024})
                    symbols_array.push_back(item);

                sizeofsymb_Array= symbols_array.size();

                for(auto item:{1,2,3,4})
                    expansion_array.push_back(item);

                sizeofexpa_Array = expansion_array.size();

                // uint32_t symbols_array [] = {16,32};
                RUN_LOOP = 200;
                erasure1 = 0;


            }

            Benchmark_Fulcrum(uint32_t Re_symobl, double erasure1, long long RUN_LOOP):
                            recoder_symbols(Re_symobl),erasure1(erasure1),RUN_LOOP(RUN_LOOP){
                
                symbols = 1600;
                symbol_size  = 1600; //1600 bytes
                // for(auto item:{16,32,64,128,256,512,1024,2048,4096,8192,})
                for(auto item:{16,32,64,128,256,512,1024})
                // for(auto item:{1600, 8000, 16000})
                // for(auto item:{16,32})
                // for(auto item:{2048})

                {
                    symbols_array.push_back(item);
                }
                sizeofsymb_Array= symbols_array.size();

                // for(auto item:{1,2,3,4})
                for(auto item:{1,2,3,4})

                    expansion_array.push_back(item);
                    
                sizeofexpa_Array = expansion_array.size();

                // uint32_t symbols_array [] = {16,32};
                // RUN_LOOP = 100;

            }
            ~Benchmark_Fulcrum(){}
            // Seed the random number generator to get different random data
            

    
            void test()
            {
                srand(static_cast<uint32_t>(time(0)));
                // Define the fulcrum encoder/decoder types that we will use
                using encoder_type = kodo_fulcrum::fulcrum_encoder<fifi::binary8>;
                // using recoder_type = kodo_fulcrum::fulcrum_inner_decoder<fifi::binary>; // recoder
                // using encoder_type = kodo_fulcrum::fulcrum_outer_decoder<fifi::binary8>;
                // using decoder_type_RLNC = kodo_rlnc::decoder<fifi::binary8>;
                using decoder_type_combi = kodo_fulcrum::fulcrum_combined_decoder<fifi::binary8>;
                using decoder_type_outer = kodo_fulcrum::fulcrum_outer_decoder<fifi::binary8>;
                using decoder_type_inner = kodo_fulcrum::fulcrum_inner_decoder<fifi::binary>;

                TimeCounter tcounter;

                uint32_t enc_index = 0, enc_index_temp = 0;
                uint32_t rec_index = 0, rec_index_temp = 0;
                uint32_t dec_inner_index = 0;
                uint32_t dec_outer_index = 0;
                uint32_t dec_combi_index = 0;

                // uint32_t dec_inner_index_temp = 0;
                // uint32_t dec_outer_index_temp = 0;
                // uint32_t dec_combi_index_temp = 0;
                // uint32_t dec_RLNC_index = 0;
                double nEnc_inner_temp,nEnc_outer_temp,nEnc_combi_temp;
                double nDec_inner_temp,nDec_outer_temp,nDec_combi_temp;

                // double erasure1 = 0.1;
                double erasure2 = 0.1;
                bool show_codeTime_flag = false;
              
                double en_total_time_temp,en_encod_time_temp;
                double de_inner_time_temp,de_outer_time_temp,de_combi_time_temp;
                double de_inner_rate_temp,de_outer_rate_temp,de_combi_rate_temp;

                double en_inner_through,en_outer_through,en_combi_through;
                double en_inner_through_temp,en_outer_through_temp,en_combi_through_temp;
                double re_through;

                double de_inner_through,de_outer_through,de_combi_through;//,de_RLNC_through;
                double de_inner_loss,de_outer_loss,de_combi_loss;//,de_RLNC_through;

                double en_temp, re_temp;
                double de_inner_through_temp,de_combi_through_temp,de_outer_through_temp;//, de_RLNC_temp;
                double de_inner_loss_temp,de_combi_loss_temp,de_outer_loss_temp;//, de_RLNC_temp;

                double en_inner_total_time,en_outer_total_time,en_combi_total_time;


                long long success_inner_received_packets = 0;
                long long success_outer_received_packets = 0;
                long long success_combi_received_packets = 0;

                
                //output the result
                // 
                std::ofstream output;
                std::stringstream iss; 

                // time_t t = time(0);
                // struct tm * now = localtime(&t);
                // iss << "simFulcrum" << "(" << now->tm_mon + 1 
                //             << now->tm_mday << now->tm_hour 
                //             << now->tm_min << now->tm_sec << ")_run"<<RUN_LOOP <<".py";
                iss << "simFulcrum" << "_loss_" << erasure1 << "_run"<<RUN_LOOP <<".csv";
                output.open(iss.str().c_str());
                

                // /*sybmols is set to be the max symbols*/
                // In the following we will make an encoder/decoder factory.
                // The factories are used to build actual encoders/decoders
                // encoder_type::factory encoder_factory(symbols, symbol_size);
                encoder_type::factory encoder_factory(symbols, symbol_size);
                
                decoder_type_inner::factory decoder_factory_inner(symbols, symbol_size);
                decoder_type_combi::factory decoder_factory_combi(symbols, symbol_size);
                decoder_type_outer::factory decoder_factory_outer(symbols, symbol_size);

                // recoder_type::factory recoder_factory(symbols, symbol_size);

                // recoder_factory.set_recoder_symbols(recoder_symbols);
                output << "Expansion, Symbols, RUN_loop, "
                        << "Enc_inner_Throughput, Enc_combi_Throughput, Enc_outer_Throughput,"
                        << "Dec_inner_Throughput, Dec_combi_Throughput, Dec_outer_Throughput,"
                        << "Dec_inner_Possibility, Dec_combi_Possibility, Dec_outer_Possibility, "

                        << "nSent_inner_Packet, nSent_combi_Packet, nSent_outer_Packet, "
                        << "nRecv_inner_Packets, nRec_combi_Packets, nRec_outer_Packets,"
                        << "Total_Time,"
                        << "Encoder_inner_Time, Encoder_combi_Time, Encoder_outer_Time, "
                        << "Decoder_inner_Time, Decoder_combi_Time, Decoder_outer_Time,"
                        << "Pac_inner_loss,Pac_combi_loss,Pac_outer_loss\n";
                        

                int index_expansion = -1;
                int index_symbols = -1;
                for(auto expansion:expansion_array)
                {   
                    index_expansion++; // begin at 0
                    // Before building the encoder, you can change the number of expansion
                    // symbols like this:
                    // encoder_factory.set_expansion(2);
                    // set expansion 
                    encoder_factory.set_expansion(expansion);
                    decoder_factory_inner.set_expansion(expansion);
                    decoder_factory_combi.set_expansion(expansion);
                    decoder_factory_outer.set_expansion(expansion);

                    // recoder.set_symbol_storage(expansion);

                    // output expansion
                    // output << "\"expansion" <<expansion << "\": [";
                    index_symbols = -1;
                    for(auto symbols_item:symbols_array)
                    {
                        index_symbols++;
                        // recoder_factory.set_symbols(symbols_item+expansion); // --------------------
                        
                        // recoder_factory.set_symbols(symbols_item);
                        // std::cout << "max symbols: " << recoder_factory.max_symbols(); // ------------------
                        
                        // We query the maximum number of expansion symbols for the fulcrum factory
                        // std::cout << "Max expansion of the encoder factory: "
                        //         << encoder_factory.max_expansion() << std::endl;
                        encoder_factory.set_symbols(symbols_item);
                        auto encoder = encoder_factory.build();
                        encoder->set_systematic_off(); // #

                        // Get the number of expansion symbols on the fulcrum encoder
                        std::cout << "Expansion symbols on the fulcrum encoder : "
                                << encoder->expansion() << std::endl;

                        
                        decoder_factory_inner.set_symbols(symbols_item);
                        decoder_factory_combi.set_symbols(symbols_item);
                        decoder_factory_outer.set_symbols(symbols_item);

                        // recoder_factory.set_symbols(symbols_item);

                        // Allocate some storage for a "payload" the payload is what we would
                        // eventually send over a network
                        std::vector<uint8_t> payload(encoder->payload_size());
                        std::vector<uint8_t> temp_payload(encoder->payload_size());
                        

                        // Allocate some data to encode. In this case we make a buffer
                        // with the same size as the encoder's block size (the max.
                        // amount a single encoder can encode)
                        std::vector<uint8_t> data_in(encoder->block_size());

                        // Just for fun - fill the data with random data
                        std::generate(data_in.begin(), data_in.end(), rand);

                        auto decoder_inner = decoder_factory_inner.build();
                        auto decoder_outer = decoder_factory_outer.build();
                        auto decoder_combi = decoder_factory_combi.build();
                        
                        // auto recoder = recoder_factory.build();

                        // std::vector<uint8_t> data_relay(recoder->block_size());
                        // recoder->copy_from_symbols(storage::storage(data_relay));
                        // std::vector<uint8_t> payload_relay(encoder->payload_size());

                        // Assign the data buffer to the encoder so that we may start
                        // to produce encoded symbols
                        encoder->set_const_symbols(storage::storage(data_in));

                        

                        // en_through = 0; 
                        en_inner_through = 0;
                        en_outer_through = 0;
                        en_combi_through = 0;
                        
                        re_through = 0; 
                        de_inner_through = 0;
                        de_outer_through = 0;
                        de_combi_through = 0;
                        // de_RLNC_through = 0;
                        de_inner_loss = 0;
                        de_outer_loss = 0;
                        de_combi_loss = 0;//,de_RLNC_through;
                        for(long long i = 0;i<RUN_LOOP;i++)
                        {   
                            // total_en_time = 0, total_de_time = 0, total_re_time = 0;

                            decoder_inner = decoder_factory_inner.build();
                            decoder_outer = decoder_factory_outer.build();
                            decoder_combi = decoder_factory_combi.build();
                           

                            // en_inner_through_temp = 0;
                            // en_outer_through_temp = 0;
                            // en_combi_through_temp = 0;

                            success_inner_received_packets = 0;
                            success_outer_received_packets = 0;
                            success_combi_received_packets = 0;

                            nEnc_inner_temp = 0;
                            nEnc_outer_temp = 0;
                            nEnc_combi_temp = 0;

                            rec_index_temp = 0;

                            nDec_inner_temp = 0;
                            nDec_outer_temp = 0;
                            nDec_combi_temp = 0;

                            en_inner_total_time = 0.0;
                            en_outer_total_time = 0.0;
                            en_combi_total_time = 0.0;

                            tcounter.clear();
                            tcounter.total_coding.start();
                            while ((!decoder_inner->is_complete())  || (!decoder_combi->is_complete())|| 
                                    (!decoder_outer->is_complete()) )
                            // while ((!decoder_outer->is_complete()))     
                            {
                                
                                // Encode a packet into the payload buffer
                                tcounter.encoder.start();
                                encoder->write_payload(payload.data());
                                tcounter.encoder.stop();

                                if((!decoder_inner->is_complete())) {nEnc_inner_temp++; en_inner_total_time=tcounter.encoder.time();}
                                if((!decoder_outer->is_complete())) {nEnc_outer_temp++; en_outer_total_time=tcounter.encoder.time();}
                                if((!decoder_combi->is_complete())) {nEnc_combi_temp++; en_combi_total_time=tcounter.encoder.time();}

                                    

                                // if ((double) rand() / (RAND_MAX) < erasure1)  //  loss=20%
                                // {
                                //     continue;
                                // }

                                // rec_index_temp++;
                                // // Encode a packet into the payload buffer
                                // tcounter.recoding.start();
                                // recoder->read_payload(payload.data());

                                // recoder->nested()->write_payload(payload.data());
                                // tcounter.recoding.stop();
                                
                                

                                if ((double) rand() / (RAND_MAX) < erasure1)  //  loss=20%
                                {
                                    continue;
                                }

                                std::copy_n(payload.data(), encoder->payload_size(), temp_payload.data());
                                
                                if((!decoder_inner->is_complete())){
                                    // Pass that packet to the decoder
                                    tcounter.inner_decoder.start();
                                    decoder_inner->read_payload(payload.data());
                                    tcounter.inner_decoder.stop();
                                    nDec_inner_temp++;
                                }

                                std::copy_n(temp_payload.data(), encoder->payload_size(), payload.data());

                                if((!decoder_outer->is_complete())){
                                    // Pass that packet to the decoder
                                    tcounter.outer_decoder.start();
                                    decoder_outer->read_payload(payload.data());
                                    tcounter.outer_decoder.stop();
                                    nDec_outer_temp++;
                              

                                }

                                std::copy_n(temp_payload.data(), encoder->payload_size(), payload.data());
                                if((!decoder_combi->is_complete())){
                                    // Pass that packet to the decoder
                                    tcounter.combi_decoder.start();
                                    decoder_combi->read_payload(payload.data());
                                    tcounter.combi_decoder.stop();
                                    nDec_combi_temp++;
                              

                                }
                                
                            }
                            tcounter.total_coding.stop();

                            double fact = 1000; // MB
                            en_inner_through_temp = (double) nEnc_inner_temp*(payload.size()*fact)/(double)(en_inner_total_time); 
                            en_outer_through_temp = (double) nEnc_outer_temp*(payload.size()*fact)/(double)(en_outer_total_time); 
                            en_combi_through_temp = (double) nEnc_combi_temp*(payload.size()*fact)/(double)(en_combi_total_time); 

                            // en_outer_through_temp = (double) nEnc_outer_temp*(payload.size())/(tcounter.total_coding.time()*fact); 


                            re_temp = (double) rec_index_temp*(payload.size()*fact)/(double)(tcounter.recoding.time());

                            de_inner_through_temp = (double) nDec_inner_temp*(payload.size()*fact)/(double)(tcounter.inner_decoder.time());
                            de_outer_through_temp = (double) nDec_outer_temp*(payload.size()*fact)/(double)(tcounter.outer_decoder.time());
                            de_combi_through_temp = (double) nDec_combi_temp*(payload.size()*fact)/(double)(tcounter.combi_decoder.time());

                            // de_outer_through_temp = (double) nDec_outer_temp*(payload.size())/(tcounter.total_coding.time()*fact);


                            de_inner_loss_temp = 1- nDec_inner_temp/ (double) nEnc_inner_temp;
                            de_outer_loss_temp = 1- nDec_outer_temp/ (double) nEnc_outer_temp;
                            de_combi_loss_temp = 1- nDec_combi_temp/ (double) nEnc_combi_temp;

                            de_inner_rate_temp =  symbols_item/ (double) nDec_inner_temp;
                            de_outer_rate_temp =  symbols_item/ (double) nDec_outer_temp;
                            de_combi_rate_temp =  symbols_item/ (double) nDec_combi_temp;

                            
                            en_total_time_temp = (double) tcounter.total_coding.time();
                            en_encod_time_temp = (double) tcounter.encoder.time();
                            de_inner_time_temp = (double) tcounter.inner_decoder.time();
                            de_outer_time_temp = (double) tcounter.outer_decoder.time();
                            de_combi_time_temp = (double) tcounter.combi_decoder.time();

                            en_inner_through += en_inner_through_temp;
                            en_outer_through += en_outer_through_temp;
                            en_combi_through += en_combi_through_temp;
                            re_through += re_temp;
                            de_inner_through += de_inner_through_temp;
                            de_outer_through += de_outer_through_temp;
                            de_combi_through += de_combi_through_temp;

                            de_inner_loss += de_inner_loss_temp;
                            de_outer_loss += de_outer_loss_temp;
                            de_combi_loss += de_combi_loss_temp;

                            // dec_inner_index += nDec_inner_temp;
                            // dec_outer_index += nDec_outer_temp; 
                            // dec_combi_index += nDec_combi_temp; 

                            // enc_index += enc_index_temp;
                            // rec_index += rec_index_temp;

                            // en_through += en_temp;
                            // re_through += re_temp;
                            // de_through += de_temp;


                            // std::cout << "en_through " << en_through << "de_through" << de_through << std::endl;
                            
                            // std::vector<uint8_t> data_out_inner(decoder_inner->block_size());
                            // std::vector<uint8_t> data_out_outer(decoder_outer->block_size());
                            std::vector<uint8_t> data_out_inner(decoder_inner->block_size());
                            std::vector<uint8_t> data_out_outer(decoder_outer->block_size());
                            std::vector<uint8_t> data_out_combi(decoder_combi->block_size());


                            // decoder_inner->copy_from_symbols(storage::storage(data_out_inner));
                            // decoder_outer->copy_from_symbols(storage::storage(data_out_outer));
                            decoder_inner->copy_from_symbols(storage::storage(data_out_inner));
                            decoder_outer->copy_from_symbols(storage::storage(data_out_outer));
                            decoder_combi->copy_from_symbols(storage::storage(data_out_combi));
                            
                        //    output << "Expansion, Symbols, RUN_loop, "
                                    // << "Enc_inner_Throughput, Enc_combi_Throughput, Enc_outer_Throughput,"
                                    // << "Dec_inner_Throughput, Dec_combi_Throughput, Dec_outer_Throughput,"
                                    // << "Dec_inner_Possibility, Dec_combi_Possibility, Dec_outer_Possibility, "

                                    // << "nSent_inner_Packet, nSent_combi_Packet, nSent_outer_Packet, "
                                    // << "nRecv_inner_Packets, nRec_combi_Packets, nRec_outer_Packets,"
                                    // << "Total_Time,"
                                    // << "Encoder_inner_Time, Encoder_combi_Time, Encoder_outer_Time, "
                                    // << "Decoder_inner_Time, Decoder_combi_Time, Decoder_outer_Time,"
                                    // << "Pac_inner_loss,Pac_combi_loss,Pac_outer_loss\n";
                                    
                            output << expansion <<","<<symbols_item<<","<<RUN_LOOP<<","
                                << en_inner_through_temp << "," << en_combi_through_temp << "," << en_outer_through_temp <<","
                                << de_inner_through_temp <<","  << de_combi_through_temp << "," << de_outer_through_temp<<","
                                << de_inner_rate_temp    <<","  << de_combi_rate_temp    << "," << de_outer_rate_temp<<","

                                << nEnc_inner_temp <<"," << nEnc_combi_temp<<","<< nEnc_outer_temp<<","
                                << nDec_inner_temp <<","<< nDec_combi_temp<<","<< nDec_outer_temp<<","
                                << tcounter.total_coding.time()*fact<<","
                                << en_inner_total_time/fact<<","<< en_combi_total_time*fact<<","<< en_outer_total_time*fact<<","
                                << tcounter.inner_decoder.time()/fact<<"," << tcounter.combi_decoder.time()/fact<<"," << tcounter.outer_decoder.time()/fact<<","
                                << de_inner_loss_temp <<","<< de_combi_loss_temp <<","<< de_outer_loss_temp <<"\n";
                                        
                            // Check if we properly decoded the data
                            

                            if (std::equal(data_out_outer.begin(), data_out_outer.end(), data_in.begin())) 
                                success_outer_received_packets++;
                            else  std::cout << "Unexpected failure to outer decode, " << "please file a bug report :)" << std::endl;
                            

                            // if (std::equal(data_out_inner.begin(), data_out_inner.end(), data_in.begin()))
                            //     success_inner_received_packets++;
                            // else std::cout << "Unexpected failure to inner decode, " << "please file a bug report :)" << std::endl;

                            // if (std::equal(data_out_combi.begin(), data_out_combi.end(), data_in.begin())) 
                            //     success_combi_received_packets++;
                            // else std::cout << "Unexpected failure to combi decoder, " << "please file a bug report :)" << std::endl;


                        }


                        de_inner_through = (double) de_inner_through/RUN_LOOP;
                        de_outer_through = (double) de_outer_through/RUN_LOOP;
                        de_combi_through = (double) de_combi_through/RUN_LOOP;
   

                        de_inner_loss = (double) de_inner_loss/RUN_LOOP; 
                        de_outer_loss = (double) de_outer_loss/RUN_LOOP; 
                        de_combi_loss = (double) de_combi_loss/RUN_LOOP;

                 

                        

                        // de_RLNC_through_point[index_expansion][index_symbols].push_back(de_RLNC_temp);

                        std::cout <<  "EncoderThroughtput inner|outer|combi "
                                //   << (double) enc_index*(payload.size())*8*RUN_LOOP/total_en_time << " Gb/s " 
                                    << en_inner_through << "|" << en_outer_through<< "|"<< en_combi_through << " MB/s " 

                                    << "Recoder Throughtput "
                                //   << (double) rec_index*(payload.size())*8*RUN_LOOP/total_re_time << " Gb/s "
                                    << re_through << " MB/s " 
                                    << "Recodered Packet = " << re_through << "\n"

                                    
                                    << "Decoder Throughtput: " << std::endl
                                //   << (double) dec_index*(payload.size())*8*RUN_LOOP/total_de_time << " Gb/s "
                                    << de_inner_through << " MB/s " 
                                    << "symbols | coded packets|  Innerdecoder Packet = " 
                                    << symbols_item<<"|"<< enc_index/RUN_LOOP<<"|" <<dec_inner_index/RUN_LOOP
                                    << " InnerDec Possibility " << de_inner_loss <<std::endl
                                    
                                    << de_outer_through << " MB/s " 
                                    << "symbols | coded packets|  Outerdecoder Packet = " 
                                    << symbols_item<<"|"<< enc_index/RUN_LOOP<<"|" << dec_outer_index/RUN_LOOP
                                    << " OuterDec Possibility " << de_outer_loss <<std::endl

                                    << de_combi_through << " MB/s " 
                                    << "symbols | coded packets|  Combidecoder Packet = " 
                                    << symbols_item<<"|"<< enc_index/RUN_LOOP<<"|" << dec_combi_index/RUN_LOOP 
                                    <<" CombiDec Possibility " << de_combi_loss <<std::endl
                                    
                                    
                                    << std::endl;



                    }

                }
                std::cout << std::endl;
                std::cout << "Chan.1 loss = "<< erasure1*100<<"% | " 
                            << "Chan.2 loss = "<< erasure2*100<<"% " 
                            // << "Total pakcets = " << symbol_size << " "
                            << "Symbol = " << symbols << " | "
                            << "Symbol_size = " << symbol_size << " "
                            // << "Enc Expansion = " << encoder->expansion()
                            // << "Dec Expansion = " << decoder->expansion()
                            << std::endl;


                output.close();
            }

    
    
};