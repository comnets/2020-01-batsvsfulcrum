/*
 Copyright (C) 2019 Yicong Su

 This file is part of SimBATS version of Yicong.
 
 SimBATS is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 SimBATS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with SimBATS.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstring>
#include <ctime>

#include "LT-RSDistribution.hpp"
#include "outPutToFile.hpp"

using namespace std;
// generate the Robust solition Distribution 
// pakcet number K

// void get_outfile(int M, double* distribution){

//     ofstream outfile;
//     string name="simRankDistM"+to_string(M)+"m8.txt";
//     outfile.open(name);

//     for(int i=0;i<M;i++){
        
//         outfile << "   "
//                   <<setiosflags(ios::scientific)
//                   << setprecision(7)
//                   << distribution[i] 
//                   << "";
//         // if((i+1)%10 == 0)
//         //     std::cout<< std::endl;
//     }
//     outfile.close();
// }

void RankDis_generate_Predefine()
{   
    int M = 16;
    double* distribution = new double[M];
    double beta = 0.0;

    memset(distribution,0,M);
    distribution[5]=0.0001;
    distribution[6]=0.0004;
    distribution[7]=0.0025;
    distribution[8]=0.0110;
    distribution[9]=0.0387;
    distribution[10]=0.1040;
    distribution[11]=0.2062;
    distribution[12]=0.2797;
    distribution[13]=0.2339;
    // distribution[38]=0.0293;
    distribution[14]=0.1038;
    distribution[15]=0.0190;
    distribution[16]=0.0008;
  

    get_outfile_Rank(M, distribution,"Rank16");


}

void RankDis_generate(int M)
{
    double* distribution = new double[M];
    double* temp = new double[M];
    double beta = 0.0;

    srand(time(NULL));
    for(int i=0;i<M;i++)
        temp[i]=(double)rand()/RAND_MAX;
    distribution[0]=1;
  

    get_outfile_ISD(M, distribution,"Rank"+to_string(M));


}