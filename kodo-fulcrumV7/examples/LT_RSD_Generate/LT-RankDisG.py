#! /usr/bin/env python3
# encoding: utf-8

import random
import matplotlib.pyplot as plt

def RankDis_generate(M):
    temp = [random.uniform(0,1) for _ in range(M-2)]
    # print(temp)
    temp = sorted(temp)
    temp=[1]+temp
    temp=temp+[1]
    print(temp)

    sum=0
    distrbution = [temp[i]-temp[i-1] for i in range(1,M)]
    print("1",distrbution)
    distrbution = [0] +distrbution
    print("2",distrbution)
    distrbution[1]=-1*distrbution[1]
    for i in range(M):
        sum+=distrbution[i]
    print("sum1:",sum)

    distrbution_final=[distrbution[i]/sum for i in range(M)]
    print("distrbution_final",distrbution_final)
    sum=0

    for i in range(M):
        sum+=distrbution_final[i]
    print("sum2:",sum)

    # t=range(M)
    # plt.subplots()
    # plt.plot(distrbution_final)
    # plt.show()

    filename="simRankDistM"+str(M)+"m8.txt"
    # with open(filename,'w+') as of:
    #     of.write("   ")
    #     for i in range(M):
    #         of.write(str(distrbution[i]))
    #         of.write("   ")

    with open(filename,'r') as inputfile:
        data=[float(x) for x in inputfile.read().split()]

    print("data:",data)
    plt.subplot(211)
    plt.plot(data,marker="o")
    plt.subplot(212)
    plt.plot(distrbution_final, marker="*")
    plt.show()

if __name__ == "__main__":
    RankDis_generate(32)