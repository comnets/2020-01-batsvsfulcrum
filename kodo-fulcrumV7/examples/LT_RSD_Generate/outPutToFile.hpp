/*
 Copyright (C) 2019 Yicong Su

 This file is part of SimBATS version of Yicong.
 
 SimBATS is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 SimBATS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with SimBATS.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstring>

using namespace std;
void get_outfile_Rank(int K, double* distribution, string appendName){

    bool scientific_flag = true;
    bool rough_flag = false;
    ofstream outfile;
    string name="./Rankfile/simRankDistM"+to_string(K)+"m8.txt";
    outfile.open(name);

    if(rough_flag)
        outfile <<"[";
    for(int i=0;i<K;i++){
        

        if(scientific_flag)
        {   
            if(i==K-1)
            {
                outfile << " "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i] ;
            }
            else{
                outfile << " "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i];
                if(rough_flag)
                        outfile << ",";
            }
            
        }
        else{
            if(i == K-1){
                outfile << " "
                    << distribution[i] ;
            }
            else
            {
                outfile << " "
                    << distribution[i];
                if(rough_flag)
                    outfile << ",";
            }
            
        }
        
        // if((i+1)%10 == 0)
        //     std::cout<< std::endl;
    }
    if(rough_flag)
        outfile <<"]";
    outfile.close();
}
void get_outfile_ISD(int K, double* distribution, string appendName){

    bool scientific_flag = true;
    bool rough_flag = false;
    ofstream outfile;
    string name="./ISDfile/ISD_K"+to_string(K)+""+appendName+".txt";
    outfile.open(name);

    if(rough_flag)
        outfile <<"[";
    for(int i=0;i<K;i++){
        

        if(scientific_flag)
        {   
            if(i==K-1)
            {
                outfile << " "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i] ;
            }
            else{
                outfile << " "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i];
                if(rough_flag)
                        outfile << ",";
            }
            
        }
        else{
            if(i == K-1){
                outfile << " "
                    << distribution[i] ;
            }
            else
            {
                outfile << " "
                    << distribution[i];
                if(rough_flag)
                    outfile << ",";
            }
            
        }
        
        // if((i+1)%10 == 0)
        //     std::cout<< std::endl;
    }
    if(rough_flag)
        outfile <<"]";
    outfile.close();
}



void get_outfile_RSD_dir_x10(int K, double* distribution,double item,std::string dir,int number){

    bool scientific_flag = true;
    bool rough_flag = false;

    ostringstream output;
    output.setf(ios::right);
    // output << fixed << setprecision(1) << item;
    output << item;
    string name;
    ofstream outfile;
    string filename;
    // string filename = "RSD_K"+to_string(K)+"delta"+output.str();
    if(dir=="./K256c0.1_delta/")
        filename = "RSD_K"+to_string(K)+"_delta"+to_string(number);
    else if(dir=="./K256delta0.1_c/")
        filename = "RSD_K"+to_string(K)+"_c"+to_string(number);

    
    name=dir+filename+".txt";
    
    outfile.open(name);

    if(rough_flag)
        outfile <<"[";
    for(int i=0;i<K;i++){
        

        if(scientific_flag)
        {   
            if(i==K-1)
            {
                outfile << "   "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i] ;
            }
            else{
                outfile << "   "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i];
                if(rough_flag)
                        outfile << ",";
            }
            
        }
        else{
            if(i == K-1){
                outfile << "   "
                    << distribution[i] ;
            }
            else
            {
                outfile << "   "
                    << distribution[i];
                if(rough_flag)
                    outfile << ",";
            }
            
        }
        
        // if((i+1)%10 == 0)
        //     std::cout<< std::endl;
    }
    if(rough_flag)
        outfile <<"]";
        
    outfile.close();
}

void get_outfile_RSD_dir(int K, double* distribution,double item,std::string dir){

    bool scientific_flag = true;
    bool rough_flag = false;

    ostringstream output;
    output.setf(ios::right);
    // output << fixed << setprecision(1) << item;
    output << item;
    string name;
    ofstream outfile;
    string filename;
    // string filename = "RSD_K"+to_string(K)+"delta"+output.str();
    if(dir=="./K256c0.1_delta/")
        filename = "RSD_K"+to_string(K)+"_delta"+output.str();
    else if(dir=="./K256delta0.1_c/")
        filename = "RSD_K"+to_string(K)+"_c"+output.str();

    
    name=dir+filename+".txt";
    
    outfile.open(name);

    if(rough_flag)
        outfile <<"[";
    for(int i=0;i<K;i++){
        

        if(scientific_flag)
        {   
            if(i==K-1)
            {
                outfile << "   "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i] ;
            }
            else{
                outfile << "   "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i];
                if(rough_flag)
                        outfile << ",";
            }
            
        }
        else{
            if(i == K-1){
                outfile << "   "
                    << distribution[i] ;
            }
            else
            {
                outfile << "   "
                    << distribution[i];
                if(rough_flag)
                    outfile << ",";
            }
            
        }
        
        // if((i+1)%10 == 0)
        //     std::cout<< std::endl;
    }
    if(rough_flag)
        outfile <<"]";
        
    outfile.close();
}
void get_outfile_RSD(int K, double* distribution,double item,int flag){

    bool scientific_flag = true;
    bool rough_flag = false;

    ostringstream output;
    output.setf(ios::right);
    // output << fixed << setprecision(1) << item;
    output << item;
    string name;
    ofstream outfile;
<<<<<<< HEAD
    // string filename = "RSDK"+to_string(K)+"delta"+output.str();
    string filename = "ISDK"+to_string(K)+"delta"+output.str();

    string name="./ISDfile/"+filename+".txt";
=======
    string filename = "RSD_K"+to_string(K)+"_delta"+output.str();
    if(flag==0)
        name="./RSDfile/"+filename+".txt";
    else 
        name="./RSDV1file/"+filename+"V1.txt";
>>>>>>> 67fd3dc829377681398d78ae0c44e52fc425fc6b
    outfile.open(name);

    if(rough_flag)
        outfile <<"[";
    for(int i=0;i<K;i++){
        

        if(scientific_flag)
        {   
            if(i==K-1)
            {
                outfile << "   "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i] ;
            }
            else{
                outfile << "   "
                        <<setiosflags(ios::scientific)
                        << setprecision(7)
                        << distribution[i];
                if(rough_flag)
                        outfile << ",";
            }
            
        }
        else{
            if(i == K-1){
                outfile << "   "
                    << distribution[i] ;
            }
            else
            {
                outfile << "   "
                    << distribution[i];
                if(rough_flag)
                    outfile << ",";
            }
            
        }
        
        // if((i+1)%10 == 0)
        //     std::cout<< std::endl;
    }
    if(rough_flag)
        outfile <<"]";
        
    outfile.close();
}