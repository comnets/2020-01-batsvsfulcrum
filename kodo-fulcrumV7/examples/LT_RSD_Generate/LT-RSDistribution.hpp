/*
 Copyright (C) 2019 Yicong Su

 This file is part of SimBATS version of Yicong.
 
 SimBATS is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 SimBATS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with SimBATS.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#include <cstdio>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstring>

#include "outPutToFile.hpp"

using namespace std;
// generate the Robust solition Distribution 
// pakcet number K

void ISD_generate(int K)
{
<<<<<<< HEAD
    double delta = 0.1;//0.0317; //0.1; //0.005;
    double c = 0.1; //0.05642;//0.1; //0.5;

    double * p_robust;

    double R = 0, beta = 0;
    double* array_roubst = new double[K];
    // double* distribution = new double[K];

    // memset(array_roubst,0,K);
    // memset(distribution,0,K);

    R = (double)c*sqrt(K)*log(K/delta);

=======
    
    double* array_ISD = new double[K];
    
>>>>>>> 67fd3dc829377681398d78ae0c44e52fc425fc6b
    //ideal solition
    // i = 1 , i = [2,K]
    array_ISD[0] = (double) 1/K;
    for(int i=2;i<=K;i++){
        array_ISD[i-1] = (double) 1/(i*(i-1));
    }

<<<<<<< HEAD


    // get_outfile(K, array_roubst);
    
    get_outfile_v1(K,array_roubst,delta);


=======
    get_outfile_ISD(K, array_ISD,"");
    
>>>>>>> 67fd3dc829377681398d78ae0c44e52fc425fc6b
}

void RSD_generate(int K)
{
    double delta = 0.1;//0.0317; //0.1; //0.005;
    double c = 0.1;//0.05642;//0.1; //0.5;

    double R = 0, beta = 0;
    double* array_roubst = new double[K];
    double* distribution = new double[K];

    // memset(array_roubst,0,K);
    memset(distribution,0,K);

    R = (double)c*log(K/delta)*sqrt(K);

    //ideal solition
    // i = 1 , i = [2,K]
    array_roubst[0] = (double) 1/K;
    for(int i=2;i<=K;i++){
        array_roubst[i-1] = (double) 1/(i*(i-1));
    }

    //robust solition
    int KdivR = (int) K/R;
    // i = 1,2,..., KdivR-1
    for(int i=1;i<=KdivR-1; i++){
        array_roubst[i-1] += (double) R/(i*K);
    }
    // i= KdivR
    array_roubst[KdivR -1] += (double) R*log(R/delta)/K;

    for(int i=0;i<K;i++){
        beta +=array_roubst[i];
    }


    // distribution part
    for(int i=0;i<K;i++){
        distribution[i] = array_roubst[i]/beta;
    }
    // int flag =0; // 0 RSDfile 1 RSDV1file
    get_outfile_RSD(K, distribution,delta,0);
    
    // get_outfile_v1(K,distribution,item);


}

void RSD_generate_deltaArray(int K,double c)
{
    // delta sure the decodign should be sucessful
    // double delta_arry []= {20,5,0.5,0.1,0.05,0.01,0.005, 0.001};//0.0317; //0.005;
    // double delta_arry []= {1,0.5,0.1,0.05,0.01,0.005,0.001,0.0001};//0.0317; //0.005;

    double delta_arry []= {0.99,0.5,0.1,0.05,0.01,0.005,0.001,0.0001};//0.0317; //0.005;
    
    // double delta_arry []= {250,5,0.5,0.1,0.05,0.01,0.005, 0.001};//0.0317; //0.005;
    // double delta_arry []= {10,0.1,0.001};//0.0317; //0.005;

    // double c = 0.1; //0.1;//0.05642; //0.5;

    double * p_robust;

    double R = 0, beta = 0;
    double* array_roubst = new double[K];
    double* distribution = new double[K];


    // int item = 0;
    for(auto delta : delta_arry)
    {   
        // memset(array_roubst,0.0,K);
        // memset(distribution,0.0,K);
        beta = 0;
        // item ++;

        R = (double)c*sqrt(K)*log(K/delta);

        //ideal solition
        // i = 1 , i = [2,K]
        array_roubst[0] = (double) 1/K;
        for(int i=2;i<=K;i++){
            array_roubst[i-1] = (double) 1/(i*(i-1));
        }

        //robust solition
        int KdivR = (int) round(K/R);
        for(int i=1;i<=KdivR-1; i++){
            array_roubst[i-1] += (double) R/(i*K);
        }
        array_roubst[KdivR -1] += (double) R*log(R/delta)/K;

        for(int i=0;i<K;i++){
            beta +=array_roubst[i];
        }


        // distribution part
        for(int i=0;i<K;i++){
            distribution[i] = (double) array_roubst[i]/beta;
        }

        // get_outfile(K, distribution);
        int flag =0; // 0 RSDfile 1 RSDV1file
        std::string dir="./K256c0.1_delta/";
        get_outfile_RSD_dir(K,distribution,delta,dir);
    }
    string filename="./K256c0.1_delta/delta_array.txt";
    ofstream outfile;
    outfile.open(filename);

    // outfile.setf(ios::right);
    // outfile.setstate
    for(auto item:delta_arry)
        outfile << item <<"   ";
    
    outfile.close();

}

void RSD_generate_cArray(int K,double delta)
{
    // delta sure the decodign should be sucessful
    // double delta_arry []= {20,5,0.5,0.1,0.05,0.01,0.005, 0.001};//0.0317; //0.005;
    // double delta_arry []= {1,0.5,0.1,0.05,0.01,0.005,0.001,0.0001};//0.0317; //0.005;

    // double c_arry []= {20,2,1,0.9,0.7,0.5,0.3,0.1,0.09,0.07,0.05,0.003,0.001,0.001};//0.0317; //0.005;
    // double c_arry []= {9,5,0.09,0.05,0.009};//0.0317; //0.005;

    // double c_arry []= {0.5,0.1,0.05,0.01,0.005, 0.001};//0.0317; //0.005;
    //20 9 5 1 0.5  0.1 0.05 0.03 0.01 0.009 
    // double c_arry []= {9,5,1, 0.9, 0.5, 0.1};//0.0317; //0.005;
    //double c_arry []= {0.1};//0.0317; //0.005;

     double c_arry []= {20,9,5,1, 0.5, 0.1,0.05, 0.03, 0.01,0.009};//0.0317; //0.005;

    // double c_arry []= {9,8,7,6,5,4,3,2,1,0.9,0.8,0.7,0.6,0.5};//0.0317; //0.005;


    // double delta_arry []= {250,5,0.5,0.1,0.05,0.01,0.005, 0.001};//0.0317; //0.005;
    // double delta_arry []= {10,0.1,0.001};//0.0317; //0.005;

    // double c = 0.1; //0.1;//0.05642; //0.5;

    double * p_robust;

    double R = 0, beta = 0;
    double* array_roubst = new double[K];
    double* distribution = new double[K];


    // int item = 0;
    // for(auto delta : delta_arry)
    for(auto c : c_arry)

    {   
        // memset(array_roubst,0.0,K);
        // memset(distribution,0.0,K);
        beta = 0;
        // item ++;

        R = (double)c*sqrt(K)*log(K/delta);

        //ideal solition
        // i = 1 , i = [2,K]
        array_roubst[0] = (double) 1/K;
        for(int i=2;i<=K;i++){
            array_roubst[i-1] = (double) 1/(i*(i-1));
        }

        //robust solition
        int KdivR = (int) round(K/R);
        for(int i=1;i<=KdivR-1; i++){
            array_roubst[i-1] += (double) R/(i*K);
        }
        array_roubst[KdivR -1] += (double) R*log(R/delta)/K;

        for(int i=0;i<K;i++){
            beta +=array_roubst[i];
        }


        // distribution part
        for(int i=0;i<K;i++){
            distribution[i] = (double) array_roubst[i]/beta;
        }

        // get_outfile(K, distribution);
        int flag =0; // 0 RSDfile 1 RSDV1file
        std::string dir="./K256delta0.1_c/";
        get_outfile_RSD_dir(K,distribution,c,dir);
    }

    string filename="./K256delta0.1_c/c_array.txt";
    ofstream outfile;
    outfile.open(filename);

    // outfile.setf(ios::right);
    // outfile.setstate
    for(auto item:c_arry)
        outfile << item <<"   ";
    
    outfile.close();

    // free(array_roubst);
    // free(distribution);

}

void RSD_generate_cArray_x10(int K,double delta)
{
    double c_arry []= {0.1};//0.0317; //0.005;


    double * p_robust;

    double R = 0, beta = 0;
    double* array_roubst = new double[K];
    double* distribution = new double[K];


    // int number = 0;
    // for(auto delta : delta_arry)
    // for(auto c : c_arry)
    double c=0.1;
    for(int number =0;number<3;number++)
    {   
        // memset(array_roubst,0.0,K);
        // memset(distribution,0.0,K);
        beta = 0;
        // item ++;

        R = (double)c*sqrt(K)*log(K/delta);

        //ideal solition
        // i = 1 , i = [2,K]
        array_roubst[0] = (double) 1/K;
        for(int i=2;i<=K;i++){
            array_roubst[i-1] = (double) 1/(i*(i-1));
        }

        //robust solition
        int KdivR = (int) round(K/R);
        for(int i=1;i<=KdivR-1; i++){
            array_roubst[i-1] += (double) R/(i*K);
        }
        array_roubst[KdivR -1] += (double) R*log(R/delta)/K;

        for(int i=0;i<K;i++){
            beta +=array_roubst[i];
        }


        // distribution part
        for(int i=0;i<K;i++){
            distribution[i] = (double) array_roubst[i]/beta;
        }

        // get_outfile(K, distribution);
        int flag =0; // 0 RSDfile 1 RSDV1file
        std::string dir="./K256delta0.1_c/";
        get_outfile_RSD_dir_x10(K,distribution,c,dir,number);
        
    }

    string filename="./K256delta0.1_c/c_array.txt";
    ofstream outfile;
    outfile.open(filename);

    // outfile.setf(ios::right);
    // outfile.setstate
    for(auto item:c_arry)
        outfile << item <<"   ";
    
    outfile.close();

    // free(array_roubst);
    // free(distribution);
}
// void RSD_generateV1_outputV1(int K,double c)
void RSD_generateV1(int K)
{
    // delta sure the decodign should be sucessful

    // double delta_arry []= {1,0.1, 0.01, 0.001, 0.0001};//0.0317; //0.005;
    
    // double delta_arry []= {1,0.5,0.1,0.05,0.01,0.005,0.001,0.0001};//0.0317; //0.005;
    // double delta_arry []= {10,0.1,0.001};//0.0317; //0.005;
    double delta_arry []= {0.1};
    double c = 0.1; //0.1;//0.05642; //0.5;

    double * p_robust;

    double R = 0, beta = 0,beta1_sum=0,temp=0;
    double* array_roubst = new double[K];
    double* distribution = new double[K];

    int flag_last = 0;

    int item = 0;
    for(auto delta : delta_arry)
    {   
        // memset(array_roubst,0.0,K);
        // memset(distribution,0.0,K);
        beta = 0;
        item ++;
        R = (double)c*sqrt(K)*log(K/delta);

        //ideal solition
        // i = 1 , i = [2,K]
        array_roubst[0] = (double) 1/K;
        for(int i=2;i<=K;i++){
            array_roubst[i-1] = (double) 1/(i*(i-1));
        }

        //robust solition
        int KdivR = (int) round(K/R);
        for(int i=1;i<=KdivR-1; i++){
            array_roubst[i-1] += (double) R/(i*K);
        }
        array_roubst[KdivR -1] += (double) R*log(R/delta)/K;

        for(int i=0;i<K;i++){
            beta +=array_roubst[i];
        }

        
        // distribution part
        for(int i=0;i<K;i++){
            distribution[i] = (double) array_roubst[i]/beta;
            
            temp+=distribution[i];
            if(distribution[i] > 0.1/K){
                flag_last = i;
                beta1_sum+=temp;
                temp = 0;
            }
            array_roubst[i] = distribution[i];
            distribution[i]=0;
        }
        
        
        for(int i=0;i<K;i++ ){
            cout<< distribution[i] <<",";
            if((i+1)%10==0)
                cout<< endl;
        }

        cout<< endl<< "flag_last/K="<<flag_last<<"/"<<K <<endl;
        // for(int i=0;i<K;i++ ){
            
        // }

        memset(distribution,0,K);
        for(int i = 0;i<=flag_last;i++)
            distribution[i] =array_roubst[i]/beta1_sum;
        // get_outfile(K, distribution);
        
        // int flag =0; // 0 RSDfile 1 RSDV1file
        get_outfile_RSD(K,distribution,delta,1);
    }

    // ostringstream ostring;
    // ostring.setf(ios::right);
    

    string filename="./DDfile/delta_array.txt";
    ofstream outfile;
    outfile.open(filename);

    // outfile.setf(ios::right);
    // outfile.setstate
    for(auto item:delta_arry)
        outfile << item <<"   ";
    
    outfile.close();
    

}

void truncated_RSD_generate(int K,int dmax)
{
    double delta = 0.0317; //0.005;
    double c = 0.05642; //0.5;

    double * p_robust;

    // int dmax = dmax;
    double R = 0, beta = 0, sum_distribution=0;
    double* array_roubst = new double[K];
    double* distribution_temp = new double[K];
    double* distribution=new double[K];

    // memset(array_roubst,0,K);
    memset(distribution_temp,0,K);
    memset(distribution,0,K);


    R = (double)c*sqrt(K)*log(K/delta);

    //ideal solition
    // i = 1 , i = [2,K]
    array_roubst[0] = (double) 1/K;
    for(int i=2;i<=K;i++){
        array_roubst[i-1] = (double) 1/(i*(i-1));
    }

    //robust solition
    int KdivR = (int) K/R;
    for(int i=1;i<=KdivR; i++){
        array_roubst[i-1] += (double) R/(i*K);
    }
    array_roubst[KdivR -1] += (double) R*log(R/delta)/K;

    for(int i=0;i<K;i++){
        beta +=array_roubst[i];
    }


    // distribution_temp part
    for(int i=0;i<KdivR;i++){
        distribution_temp[i] = array_roubst[i]/beta;
    }

    dmax = KdivR;
    // truncated_RSD_generate
    for(int i=0;i<K;i++){
        if(i<dmax-1)
        {
            distribution[i] = distribution_temp[i];
        }
        else if(i > dmax -1)
        {
            sum_distribution += distribution_temp[i] ;
        }
        
    }
    distribution[dmax-1] = sum_distribution;


    get_outfile_RSD(K, distribution,delta,0);

}


void OPD_generate(int K,double redundancy)
{
    double* array_roubst = new double[K];
    double* distribution = new double[K];
    // double R = 0;
    // R = (double)c*sqrt(K)*log(K/delta);
    // int KdivR = (int) K/R;
    double beta = 0.0;

    for(int i=2;i<=K;i++){
        array_roubst[i-1] = (double) 1/(i*(i-1));
    }
    array_roubst[0] = 0.083;
    array_roubst[1] = 0.487;
    array_roubst[99] = 0.032; // 100 -1

    for(int i=0;i<K;i++)
        beta += array_roubst[i];

    std::cout << "Robust Solition is:"<<beta<<std::endl;
    get_outfile_ISD(K, distribution,"OPD");

}

void SPD_generate_inac(int K)
{
    double* array_roubst = new double[K];
    double* distribution = new double[K];
    double beta = 0.0;

    memset(distribution,0,K);
    distribution[12]=0.2311;
    distribution[13]=0.0400;
    distribution[14]=0.1530;
    distribution[19]=0.0925;
    distribution[20]=0.0688;
    distribution[22]=0.0293;
    distribution[26]=0.0310;
    distribution[27]=0.0815;
    distribution[36]=0.0724;
    // distribution[38]=0.0293;
    distribution[38]=0.0068;
    distribution[49]=0.0652;
    distribution[70]=0.0077;
    distribution[71]=0.0507;
    distribution[115]=0.0281;
    distribution[116]=0.0149;
    distribution[255]=0.0288;

    get_outfile_ISD(K, distribution,"inac");

}

void SPD_generate_BP(int K,double redundancy)
{
    double* array_roubst = new double[K];
    double* distribution = new double[K];
    double beta = 0.0;

    memset(distribution,0,K);
    distribution[11]=0.2015;
    distribution[12]=0.000;
    distribution[13]=0.428;
    distribution[14]=0.1893;
    distribution[19]=0.0110;
    distribution[20]=0.0915;
    distribution[22]=0.00;
    distribution[26]=0.0332;
    distribution[27]=0.0872;
    // distribution[38]=0.0293;
    distribution[36]=0.0678;
    distribution[38]=0.0073;
    distribution[49]=0.0697;
    distribution[70]=0.0083;
    distribution[71]=0.0542;
    distribution[115]=0.0301;
    distribution[116]=0.0240;
    distribution[255]=0.0820;


    get_outfile_ISD(K, distribution,"BP");

}

int recursion(int d){
    if(d == 1)
        return 1;
    return recursion(d-1);
}

void possion_RSD_generateV2(int K,int dmax)
{
    double delta = 0.1;//0.0317; //0.005;
    double c = 0.1;//0.05642; //0.5;

    // int dmax = dmax;
    double R = 0, beta = 0, sum_distribution=0;
    double* array_roubst = new double[K];
    double* distribution_temp = new double[K];
    double* distribution=new double[K];

    // memset(array_roubst,0,K);
    memset(distribution_temp,0,K);
    memset(distribution,0,K);


    R = (double)c*sqrt(K)*log(K/delta);

    //ideal solition
    // i = 1 , i = [2,K]
    array_roubst[0] = (double) 1/K;
    for(int i=2;i<=K;i++){
        array_roubst[i-1] = (double) 1/(i*(i-1));
    }

    //robust solition
    int KdivR = (int) K/R;
    for(int i=1;i<=KdivR-1; i++){
        array_roubst[i-1] += (double) R/(i*K);
    }
    array_roubst[KdivR -1] += (double) R*log(R/delta)/K;

    for(int i=0;i<K;i++){
        beta +=array_roubst[i];
    }


    // distribution_temp part
    for(int i=0;i<KdivR;i++){
        distribution_temp[i] = array_roubst[i]/beta;
    }


    // truncated_RSD_generate
    for(int i=0;i<K;i++){
        if(i<dmax-1)
        {
            distribution[i] = distribution_temp[i];
        }
        else if(i > dmax -1)
        {
            sum_distribution += distribution_temp[i] ;
        }
        
    }
    distribution[dmax-1] = sum_distribution;


    get_outfile_ISD(K, distribution, "poission");

}