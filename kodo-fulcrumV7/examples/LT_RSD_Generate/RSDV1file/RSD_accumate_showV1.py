#! /usr/bin/env python3
# encoding: utf-8

import matplotlib.pyplot as plt
import numpy as np

def main():
 
    filenameArray=[]
    labelArray=[]
    for K in [16,32,64,128,256,512,1024]:
        filename="RSD_K"+str(K)+"delta0.1V1"
        filenameArray.append(filename)
        labelArray.append(filename+"_c0.1")
    # labelArray=filenameArray

    # filename=" .txt"

    dataArray=[]
    for filename in filenameArray:
        tempSum=0
        with open(filename+".txt",'r') as fi:
            data=[ float(item) for item in fi.read().split()]

        for i in range(len(data)):
            tempSum+=data[i]
            data[i]=tempSum

        dataArray.append(data)


    show(dataArray,labelArray)

def show(dataArray,labelArray):
    # markerArray = ['o','x','P','*','+','1','v','2','3','4','5','8','9','.']

    colorArray=["r","g","b"]

    fix,ax = plt.subplots()

    ##(1)###################
    ## degree distribution accumate
    ## filenameArray=["simDegreeK1600M32m8","simDegreeK8000M32m8","simDegreeK16000M32m8"] #
    ######################
    shortCutLength=100
    for i in range(len(dataArray)):
        
        # plt.plot(dataArray[i],marker=markerArray[i],label=labelArray[i])
        if(len(dataArray[i])<shortCutLength):
            xachse=np.linspace(1,len(dataArray[i]),len(dataArray[i]))
            plt.plot(xachse,dataArray[i],label=labelArray[i])
        else:
            xachse=np.linspace(1,shortCutLength,shortCutLength)
            plt.plot(xachse,dataArray[i][:shortCutLength],label=labelArray[i])
            
    plt.yscale('linear')
    

    plt.legend()
    plt.grid()
    plt.show()


def generateRank():
    
    fileName="simRankDistM32m8_0ne"

    with open(fileName+".txt","w") as fo:
        for i in range(32):
            fo.write("   "+str(float(0)))

        fo.write("   "+str(float(1)))



if __name__ == "__main__":
    main()
    # generateRank()