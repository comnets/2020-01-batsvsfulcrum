'''
@Author: your name
@Date: 2019-12-23 15:12:32
@LastEditTime : 2019-12-28 23:44:36
@LastEditors  : Please set LastEditors
@Description: In User Settings Edit
@FilePath: /BB_kodo-fulcrum_V7/examples/Z_M16_0.2_withLDPC/Cumulate_K256/averRank.py
'''
# !/usr/bin/env python3
# encoding:utf-8
import os,sys
import matplotlib.pyplot as plt
import numpy as np

# filenName="simRankDistM16m8.txt"

# for filenName in ["simRankDistM16m8","simRankDistM32m8","simRDistM32m8_loss0.1","simRankDistM32m8_loss0"]:
#     # filenName += ".txt"
#     with open(filenName+".txt",'r') as inputfile:
#         data=[float(item) for item in inputfile.read().split()]

#     print("Data:")
#     print(data)

#     average=0
#     for i in range(len(data)):
#         average+=i*data[i]
#     print("Aver Data:",str(average))

def autolabel(ax,rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{:.2f}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')





def show(averDDArray,labelArray):
    # markerArray = ['o','x','P','*','+','1','v','2','3','4','5','8','9','.']

    colorArray=["r","g","b","b"]

    fix,ax = plt.subplots()

    ##(1)###################
    ## degree distribution accumate
    ## filenameArray=["simDegreeK1600M32m8","simDegreeK8000M32m8","simDegreeK16000M32m8"] #
    ######################
    xlabels_local=["Different Degree Distribution"]
    
    factorW1=2
    factorW2=2
    width=0.2
    xachse=np.arange(len(xlabels_local))
    plt.xticks(range(len(xlabels_local)),xlabels_local)

    # plt.bar(xachse-width*3/factorW1,averDDArray[0],width/factorW2,label=labelArray[0],colar=colorArray[0])
    # plt.bar(xachse-width/factorW1,averDDArray[1],width/factorW2,label=labelArray[1],colar=colorArray[1])
    # # plt.bar(xachse,bats_RSD_Enc,width, yerr=bats_RSD_Enc_std, label="bats_RSD_Enc")
    # plt.bar(xachse+width/factorW1,averDDArray[2],width/factorW2,label=labelArray[2],colar=colorArray[2])
    # plt.bar(xachse+width*3/factorW1,averDDArray[3],width/factorW2, label=labelArray[3],colar=colorArray[3])

    rect1=ax.bar(xachse-width*3/factorW1,averDDArray[0],width/factorW2,label=labelArray[0])
    rect2=ax.bar(xachse-width/factorW1,averDDArray[1],width/factorW2,label=labelArray[1])
    # plt.bar(xachse,bats_RSD_Enc,width, yerr=bats_RSD_Enc_std, label="bats_RSD_Enc")
    rect3=ax.bar(xachse+width/factorW1,averDDArray[2],width/factorW2,label=labelArray[2])
    rect4=ax.bar(xachse+width*3/factorW1,averDDArray[3],width/factorW2, label=labelArray[3])

    
    autolabel(ax,rect1)
    autolabel(ax,rect2)
    autolabel(ax,rect3)
    autolabel(ax,rect4)


    plt.ylabel("Average Degree")  
    
    # plt.xlabel("Different Degree Distribution")

    ##(2)###################
    ## rank accumate
    ## filenameArray=["simDDK32delta0.1.txt","V1simDDK32delta0.1.txt"] #
    ######################

    # plt.stem(dataArray[0],label=labelArray[0])
    # plt.plot(dataArray[1],marker=markerArray[1],label=labelArray[1],color="orange")
    
    # plt.yscale("bar")
    plt.legend()
    plt.grid(color='gray', linestyle='-', linewidth=0.1)
    plt.show()


def main():
    
    filenameArray=[]
    for K in [16,32,64,128,256,512,1024,1600,8000,16000]:
        filenameArray.append("ISD_K"+str(K))
    # filename=" .txt"

    averDDArray=[]
    for filename in filenameArray:
        tempSum=0
        with open(filename+".txt",'r') as fi:
            data=[ float(item) for item in fi.read().split()]

        for i in range(len(data)):
            tempSum+=(i+1)*data[i]

        averDDArray.append(tempSum)

    print("ISD\n")
    print(averDDArray)
    # show(averDDArray,labelArray)
    
if __name__ == "__main__":
    main()
    