#! /usr/bin/env python3
# encoding: utf-8

import matplotlib.pyplot as plt
import numpy as np

def main():
 
    ## (1) rank accumulate
    # filenameArray=["simRankDistM32m8_0ne","simRankDistM32m8"]
    
    ## (2) K1600 K8000 K16000
    # filenameArray=["simDegreeK1600M32m8","simDegreeK8000M32m8","simDegreeK16000M32m8"]
    # filenameArray=["simDegreeK256M32m8"]
    
    ## (3) opDDK256 with different n
    # filenameArray=["opDDK256delta0.1_25","opDDK256delta0.1_50","opDDK256delta0.1_100"]

    
    ## (4) delta is not 0.1, generate by matlab 
    # filenameArray=["best_dd_K32","best_dd_K64","best_dd_K128","best_dd_K256","best_dd_K512","best_dd_K1024"]

    ## (5) K256 with different operation
    # filenameArray=["V1opDDK256delta0.1_25","opDDK256delta0.1_25","simDegreeK256M32m8","simDDK256delta0.1"]

    # filenameArray=["V1opDDK256delta0.1_25"]

    ## (6) V1simDDK
    # filenameArray=["V1simDDK256delta0.1"]

    ## (7) simDegreeK1600 8000 16000
    # filenameArray=["simDegreeK1600M32m8","simDegreeK8000M32m8","simDegreeK16000M32m8"]
    
    ## (8)
    # filenameArray=["ISD_K256_delta0.1", "RSD_K256_delta0.1","ddK256M16_Asy0.1","ddK256M16_Asy0.1_OP","ddK256M16_Asy0.01","ddK256M16_Asy0.01_OP"]
    # labelArray=["ISD", "RSD", "Asy","IANC","Asy0.01","IANC0.01"]

    # filenameArray=["ISD_K256_delta0.1","RSD_K256_delta0.1",
    #                 "ddK256M16_Asy0.1","ddK256M16_Asy0.1_OP",
    #                 # "ddK256M16_Asy0.01","ddK256M16_Asy0.01_OP"
    #                 ]

    # labelArray=   ["ISD (K=256)",  
    #                 "RSD(K=256)",
    #                "Asy (K=256)",  
    #                "INAC(K=256)",
    #             #    "Asy (delta=0.01)",  
    #             #    "IANC(delta=0.01)"
    #                ]

    filenameArray=["ISD_K64_delta0.1","RSD_K64_delta0.1",
                    "ddK64M16_Asy0.1","ddK64M16_Asy0.1_OP",
                    # "ddK256M16_Asy0.01","ddK256M16_Asy0.01_OP"
                    ]

    labelArray=   ["ISD (K=64)",  
                    "RSD(K=64)",
                   "Asy (K=64)",  
                   "INAC(K=64)",
                #    "Asy (delta=0.01)",  
                #    "IANC(delta=0.01)"
                   ]


    # filenameArray2=["ddK1024M16_Asy0.1","ddK1024M16_Asy0.1_OP",
    #                 # "ddK1024M16_Asy0.01","ddK1024M16_Asy0.01_OP"
    #                 ]
    # labelArray=2   ["Asy (K=1024)",  
    #                "INAC(K=1024)",
    #             #    "Asy (delta=0.01)",  
    #             #    "IANC(delta=0.01)"
    #                ]
    # filename=" .txt"

    dataArray=[]
    for filename in filenameArray:
        tempSum=0
        with open(filename+".txt",'r') as fi:
            data=[ float(item) for item in fi.read().split()]

        for i in range(len(data)):
            tempSum+=data[i]
            data[i]=tempSum

        dataArray.append(data)


    show(dataArray,labelArray)

def show(dataArray,labelArray):
    # markerArray = ['o','x','P','*','+','1','v','2','3','4','5','8','9','.']

    marker_array = ['o','x','^','*','>','+','v','<','3','4','5','8','9','.']
    color_array=['b','g','r','c','m','y','k','w']

    # fix,ax = plt.subplots()

    ##(1)###################
    ## degree distribution accumate
    ## filenameArray=["simDegreeK1600M32m8","simDegreeK8000M32m8","simDegreeK16000M32m8"] #
    ######################

    for i in range(len(dataArray)):
        # plt.plot(dataArray[i],marker=markerArray[i],label=labelArray[i])
        yachse=np.array(dataArray[i])
        plt.plot(yachse*100,color=color_array[i+2],label=labelArray[i])

    plt.xlabel("Number of Degree",fontsize=12)
    plt.ylabel("Cumulative Distribution [%]",fontsize=12)

    ##(2)###################
    ## rank accumate
    ## filenameArray=["simDDK32delta0.1.txt","V1simDDK32delta0.1.txt"] #
    ######################

    # plt.stem(dataArray[0],label=labelArray[0])
    # plt.plot(dataArray[1],marker=markerArray[1],label=labelArray[1],color="orange")
    

    plt.legend(fontsize=10)
    plt.grid(color='gray', linestyle='-', linewidth=0.1)
    plt.grid(b=True, which='major', color='#666666', linestyle='-')

    # Show the minor grid lines with very faint and almost transparent grey lines
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)

    plt.show()


def generateRank():
    
    fileName="simRankDistM32m8_0ne"

    with open(fileName+".txt","w") as fo:
        for i in range(32):
            fo.write("   "+str(float(0)))

        fo.write("   "+str(float(1)))



if __name__ == "__main__":
    main()
    # generateRank()